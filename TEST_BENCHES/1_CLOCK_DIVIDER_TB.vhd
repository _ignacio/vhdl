----------------------------------------------------------------------------------
-- Company:			SERNAS S.L.
-- Engineer:		IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO 
-- 
-- Create Date:		16.01.2020 18:00:07
-- Design Name:		
-- Module Name:		CLOCK_DIVIDER_TB - BEHAVIORAL
-- Project Name:	TAIOC - THE "ALL IN ONE" CHRONO
-- Target Devices:	FPGA Nexus4 DDR
-- Tool Versions:	
-- Description:		
-- 
-- Dependencies:	
-- 
-- Revision:		19.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity CLOCK_DIVIDER_TB is
end CLOCK_DIVIDER_TB;

architecture BEHAVIORAL of CLOCK_DIVIDER_TB is
	component CLOCK_DIVIDER
		generic(
			INPUT_FREQUENCY : positive	:= 100000000;
			OUTPUT_FREQUENCY : positive	:= 10000
		);
		port(
			CLOCK_DIVIDER_CLOCK_IN	: in std_logic;
			CLOCK_DIVIDER_RESET		: in std_logic;
			CLOCK_DIVIDER_CLOCK_OUT	: out std_logic
		);
	end component;
	signal CLOCK_DIVIDER_CLOCK_IN	: std_logic;
	signal CLOCK_DIVIDER_RESET		: std_logic;
	signal CLOCK_DIVIDER_CLOCK_OUT	: std_logic;
	constant TB_PERIOD				: time := 10 ns; -- EDIT Put right period here
	signal TB_CLOCK					: std_logic := '0';
	signal TB_SIMULATION_END		: std_logic := '0';
begin
	uut : CLOCK_DIVIDER
		port map(
			CLOCK_DIVIDER_CLOCK_IN		=> CLOCK_DIVIDER_CLOCK_IN,
			CLOCK_DIVIDER_RESET			=> CLOCK_DIVIDER_RESET,
			CLOCK_DIVIDER_CLOCK_OUT		=> CLOCK_DIVIDER_CLOCK_OUT
		);
	-- Clock generation
	TB_CLOCK <= not TB_CLOCK after TB_PERIOD/2 when TB_SIMULATION_END /= '1' else '0';
	-- EDIT: Check that CLOCK_DIVIDER_CLOCK_IN is really your main clock signal
	CLOCK_DIVIDER_CLOCK_IN <= TB_CLOCK;
	stimuli : process
	begin
		-- EDIT Adapt initialization as needed

		-- Reset generation
		-- EDIT: Check that CLOCK_DIVIDER_RESET is really your reset signal
		CLOCK_DIVIDER_RESET <= '1';
		wait for 100 ns;
		CLOCK_DIVIDER_RESET <= '0';
		wait for 100 ns;

		-- EDIT Add stimuli here
		wait for 100 * TB_PERIOD;

		-- Stop the clock and hence terminate the simulation
		TB_SIMULATION_END <= '1';
		wait;
	end process;
end BEHAVIORAL;