----------------------------------------------------------------------------------
-- Company:			SERNAS S.L.
-- Engineer:		IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO 
-- 
-- Create Date:		16.01.2020 17:46:05
-- Design Name:		
-- Module Name:		TOP_TB - BEHAVIORAL
-- Project Name:	TAIOC - THE "ALL IN ONE" CHRONO
-- Target Devices:	FPGA Nexus4 DDR
-- Tool Versions:	
-- Description:		
-- 
-- Dependencies:	
-- 
-- Revision:		17.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity TOP_TB is
end TOP_TB;

architecture BEHAVIORAL of TOP_TB is
	component TOP
		generic(
			TOP_O : positive := 1;--NO TOCAR
			TOP_N : positive := 4;
			TOP_M : positive := 7;--NO TOCAR
			TOP_P : positive := 8;--NO TOCAR
			TOP_CONDITIONER_SYNCHRONIZER_INDEX : positive := 2;
			TOP_CONDITIONER_DEBOUNCER_DELAY : positive := 1000;
			TOP_INPUT_FREQUENCY		: positive := 100000000;	--RELOJ PLACA 100 MHz
			TOP_GLOBAL_FREQUENCY	: positive := 100000;		--MAYOR QUE TOP_CHRONO_FREQUENCY
			TOP_CHRONO_FREQUENCY	: positive := 10000;		--MAYOR FRECUENCIA CHRONO
			TOP_DISPLAY_FREQUENCY	: positive := 400;			--FRECUENCIA DISPLAY=50*TOP_P
			TOP_CHRONO_0_LIMIT_0	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_0_LIMIT_1	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_0_LIMIT_2	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_0_LIMIT_3	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_0_LIMIT_4	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_0_LIMIT_5	: positive := 5;--POR DEFECTO PONER 5
			TOP_CHRONO_0_LIMIT_6	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_0_LIMIT_7	: positive := 5;--POR DEFECTO PONER 5
			TOP_CHRONO_0_MODE	: std_logic := '0';--POR DEFECTO PONER '0' (FORWARD)
			TOP_CHRONO_0_LOAD_0	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_0_LOAD_1	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_0_LOAD_2	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_0_LOAD_3	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_0_LOAD_4	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_0_LOAD_5	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_0_LOAD_6	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_0_LOAD_7	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_1_LIMIT_0	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_1_LIMIT_1	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_1_LIMIT_2	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_1_LIMIT_3	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_1_LIMIT_4	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_1_LIMIT_5	: positive := 5;--POR DEFECTO PONER 5
			TOP_CHRONO_1_LIMIT_6	: positive := 9;--POR DEFECTO PONER 9
			TOP_CHRONO_1_LIMIT_7	: positive := 5;--POR DEFECTO PONER 5
			TOP_CHRONO_1_MODE	: std_logic := '1';--POR DEFECTO PONER '1' (BACKWARD)
			TOP_CHRONO_1_LOAD_0	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_1_LOAD_1	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_1_LOAD_2	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_1_LOAD_3	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_1_LOAD_4	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_1_LOAD_5	: integer := 3;--<2^CHRONO_N
			TOP_CHRONO_1_LOAD_6	: integer := 0;--<2^CHRONO_N
			TOP_CHRONO_1_LOAD_7	: integer := 0--<2^CHRONO_N
		);
		port(
			TOP_CLOCK : in std_logic;
			TOP_MODE : in std_logic;
			TOP_RESET : in std_logic;
			TOP_START_PAUSE : in std_logic;
			TOP_MASTER_RESET : in std_logic;
			TOP_DIGIT_DATA : out std_logic_vector(TOP_M-1 downto 0);
			TOP_DIGIT_POSITION : out std_logic_vector(TOP_P-1 downto 0)
		);
	end component;
begin

end BEHAVIORAL;