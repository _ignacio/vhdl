----------------------------------------------------------------------------------
-- Company:			SERNAS S.L.
-- Engineer:		IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO 
-- 
-- Create Date:		16.01.2020 19:02:09
-- Design Name:		
-- Module Name:		COUNTER_TB - BEHAVIORAL
-- Project Name:	TAIOC - THE "ALL IN ONE" CHRONO
-- Target Devices:	FPGA Nexus4 DDR
-- Tool Versions:	
-- Description:		
-- 
-- Dependencies:	
-- 
-- Revision:		17.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity COUNTER_TB is
end COUNTER_TB;

architecture BEHAVIORAL of COUNTER_TB is
    component COUNTER
        generic(
		COUNTER_N : positive := 4;
		COUNTER_LIMIT : positive := 9
	);
	port(
		COUNTER_CLOCK	: in std_logic;
		COUNTER_RESET	: in std_logic;
		COUNTER_ENABLE	: in std_logic;
		COUNTER_MODE	: in std_logic;
		COUNTER_LOAD	: in std_logic_vector(COUNTER_N-1 downto 0);
		COUNTER_OUTPUT	: out std_logic_vector(COUNTER_N-1 downto 0);
		COUNTER_CARRIER	: out std_logic
	);
	end component;
	signal COUNTER_CLOCK		: std_logic;
	signal COUNTER_RESET		: std_logic;
	signal COUNTER_ENABLE		: std_logic;
	signal COUNTER_MODE			: std_logic;
	signal COUNTER_LOAD			: std_logic_vector (COUNTER_N-1 downto 0);
	signal COUNTER_OUTPUT		: std_logic_vector (COUNTER_N-1 downto 0);
	signal COUNTER_CARRIER		: std_logic;
	constant TB_PERIOD			: time := 10 ns; -- EDIT Put right period here
	signal TB_CLOCK				: std_logic := '0';
	signal TB_SIMULATION_END	: std_logic := '0';
begin
	dut : COUNTER
		port map(
			COUNTER_CLOCK   => COUNTER_CLOCK,
			COUNTER_RESET   => COUNTER_RESET,
			COUNTER_ENABLE  => COUNTER_ENABLE,
			COUNTER_MODE    => COUNTER_MODE,
			COUNTER_LOAD    => COUNTER_LOAD,
			COUNTER_OUTPUT  => COUNTER_OUTPUT,
			COUNTER_CARRIER => COUNTER_CARRIER
		);
	-- Clock generation
	TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';
	-- EDIT: Check that COUNTER_CLOCK is really your main clock signal
	COUNTER_CLOCK <= TbClock;
	stimuli : process
	begin
		-- EDIT Adapt initialization as needed
		COUNTER_ENABLE <= '0';
		COUNTER_MODE <= '0';
		COUNTER_LOAD <= (others => '0');

		-- Reset generation
		-- EDIT: Check that COUNTER_RESET is really your reset signal
		COUNTER_RESET <= '1';
		wait for 100 ns;
		COUNTER_RESET <= '0';
		wait for 100 ns;

		-- EDIT Add stimuli here
		wait for 100 * TbPeriod;

		-- Stop the clock and hence terminate the simulation
		TbSimEnded <= '1';
		wait;
	end process;
end BEHAVIORAL;