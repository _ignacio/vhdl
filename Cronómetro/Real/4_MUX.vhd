----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 03.01.2020
-- Design Name:
-- Module Name: MUX - BEHAVIORAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 14.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity MUX is
	generic(
		MUX_N : positive := 4
	);
	port(
		MUX_INPUT_SELECTION : in std_logic;
		MUX_INPUT_0 : in std_logic_vector(MUX_N-1 downto 0);
		MUX_INPUT_1 : in std_logic_vector(MUX_N-1 downto 0);
		MUX_OUTPUT : out std_logic_vector(MUX_N-1 downto 0)
	);
end entity MUX;

architecture BEHAVIORAL of MUX is
begin
	with MUX_INPUT_SELECTION select
		MUX_OUTPUT <=	MUX_INPUT_0 when '0',	--MUX_INPUT_SELECTION==0
						MUX_INPUT_1 when others;--MUX_INPUT_SELECTION==1
end architecture BEHAVIORAL;