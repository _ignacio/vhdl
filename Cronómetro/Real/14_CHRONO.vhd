----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 10.01.2020
-- Design Name:
-- Module Name: CHRONO - STRUCTURAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 16.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CHRONO is
	generic(
		CHRONO_O : positive := 1;
		CHRONO_N : positive := 4;
		CHRONO_INPUT_FREQUENCY		: positive := 100000000;
		CHRONO_WORKING_FREQUENCY	: positive := 10000;
		CHRONO_0_LIMIT_0	: positive := 9;
		CHRONO_0_LIMIT_1	: positive := 9;
		CHRONO_0_LIMIT_2	: positive := 9;
		CHRONO_0_LIMIT_3	: positive := 9;
		CHRONO_0_LIMIT_4	: positive := 9;
		CHRONO_0_LIMIT_5	: positive := 5;
		CHRONO_0_LIMIT_6	: positive := 9;
		CHRONO_0_LIMIT_7	: positive := 5;
		CHRONO_0_MODE	: std_logic := '0';
		CHRONO_0_LOAD_0	: positive := 0;--<2^CHRONO_N
		CHRONO_0_LOAD_1	: positive := 0;--<2^CHRONO_N
		CHRONO_0_LOAD_2	: positive := 0;--<2^CHRONO_N
		CHRONO_0_LOAD_3	: positive := 0;--<2^CHRONO_N
		CHRONO_0_LOAD_4	: positive := 0;--<2^CHRONO_N
		CHRONO_0_LOAD_5	: positive := 0;--<2^CHRONO_N
		CHRONO_0_LOAD_6	: positive := 0;--<2^CHRONO_N
		CHRONO_0_LOAD_7	: positive := 0;--<2^CHRONO_N
		CHRONO_1_LIMIT_0	: positive := 9;
		CHRONO_1_LIMIT_1	: positive := 9;
		CHRONO_1_LIMIT_2	: positive := 9;
		CHRONO_1_LIMIT_3	: positive := 9;
		CHRONO_1_LIMIT_4	: positive := 9;
		CHRONO_1_LIMIT_5	: positive := 5;
		CHRONO_1_LIMIT_6	: positive := 9;
		CHRONO_1_LIMIT_7	: positive := 5;
		CHRONO_1_MODE	: std_logic := '1';
		CHRONO_1_LOAD_0	: positive := 0;--<2^CHRONO_N
		CHRONO_1_LOAD_1	: positive := 0;--<2^CHRONO_N
		CHRONO_1_LOAD_2	: positive := 0;--<2^CHRONO_N
		CHRONO_1_LOAD_3	: positive := 0;--<2^CHRONO_N
		CHRONO_1_LOAD_4	: positive := 0;--<2^CHRONO_N
		CHRONO_1_LOAD_5	: positive := 3;--<2^CHRONO_N
		CHRONO_1_LOAD_6	: positive := 0;--<2^CHRONO_N
		CHRONO_1_LOAD_7	: positive := 0--<2^CHRONO_N
	);
	port(
		CHRONO_CLOCK : in std_logic;
		CHRONO_MODE : in std_logic;
		CHRONO_RESET : in std_logic;
		CHRONO_START_PAUSE : in std_logic;
		CHRONO_MASTER_RESET : in std_logic;
		CHRONO_OUTPUT_0 : out std_logic_vector(CHRONO_N-1 downto 0);
		CHRONO_OUTPUT_1 : out std_logic_vector(CHRONO_N-1 downto 0);
		CHRONO_OUTPUT_2 : out std_logic_vector(CHRONO_N-1 downto 0);
		CHRONO_OUTPUT_3 : out std_logic_vector(CHRONO_N-1 downto 0);
		CHRONO_OUTPUT_4 : out std_logic_vector(CHRONO_N-1 downto 0);
		CHRONO_OUTPUT_5 : out std_logic_vector(CHRONO_N-1 downto 0);
		CHRONO_OUTPUT_6 : out std_logic_vector(CHRONO_N-1 downto 0);
		CHRONO_OUTPUT_7 : out std_logic_vector(CHRONO_N-1 downto 0)
	);
end entity CHRONO;

architecture STRUCTURAL of CHRONO is
	component  CLOCK_DIVIDER
		generic(
			INPUT_FREQUENCY : positive := 100000000;
			OUTPUT_FREQUENCY : positive := 10000
		);
		port(
			CLOCK_DIVIDER_CLOCK_IN : in std_logic;
			CLOCK_DIVIDER_RESET : in std_logic;
			CLOCK_DIVIDER_CLOCK_OUT : out std_logic
		);
	end component;
	component  COUNTER_DEMUX
		generic(
			COUNTER_DEMUX_N : positive := 1
		);
		port(
			COUNTER_DEMUX_OUTPUT_SELECTION : in std_logic;
			COUNTER_DEMUX_INPUT_0 : in std_logic_vector(COUNTER_DEMUX_N-1 downto 0);
			COUNTER_DEMUX_INPUT_1 : in std_logic_vector(COUNTER_DEMUX_N-1 downto 0);
			COUNTER_DEMUX_OUTPUT_0_0 : out std_logic_vector(COUNTER_DEMUX_N-1 downto 0);
			COUNTER_DEMUX_OUTPUT_0_1 : out std_logic_vector(COUNTER_DEMUX_N-1 downto 0);
			COUNTER_DEMUX_OUTPUT_1_0 : out std_logic_vector(COUNTER_DEMUX_N-1 downto 0);
			COUNTER_DEMUX_OUTPUT_1_1 : out std_logic_vector(COUNTER_DEMUX_N-1 downto 0)
		);
	end component;
	component FLIP_FLOP_X
		port(
			FLIP_FLOP_X_CLOCK : in std_logic;
			FLIP_FLOP_X_INPUT : in std_logic;
			FLIP_FLOP_X_RESET : in std_logic;
			FLIP_FLOP_X_OUTPUT : out std_logic
		);
	end component ;
	component  CHRONO_COUNTER
		generic(
			CHRONO_COUNTER_N		: positive := 4;
			CHRONO_COUNTER_LIMIT_0	: positive := 9;
			CHRONO_COUNTER_LIMIT_1	: positive := 9;
			CHRONO_COUNTER_LIMIT_2	: positive := 9;
			CHRONO_COUNTER_LIMIT_3	: positive := 9;
			CHRONO_COUNTER_LIMIT_4	: positive := 9;
			CHRONO_COUNTER_LIMIT_5	: positive := 5;
			CHRONO_COUNTER_LIMIT_6	: positive := 9;
			CHRONO_COUNTER_LIMIT_7	: positive := 5
		);
		port(
			CHRONO_COUNTER_MODE : in std_logic;
			CHRONO_COUNTER_CLOCK : in std_logic;
			CHRONO_COUNTER_RESET : in std_logic;
			CHRONO_COUNTER_ENABLE : in std_logic;
			CHRONO_COUNTER_LOAD_0 : in std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_LOAD_1 : in std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_LOAD_2 : in std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_LOAD_3 : in std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_LOAD_4 : in std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_LOAD_5 : in std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_LOAD_6 : in std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_LOAD_7 : in std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_OUTPUT_0 : out std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_OUTPUT_1 : out std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_OUTPUT_2 : out std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_OUTPUT_3 : out std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_OUTPUT_4 : out std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_OUTPUT_5 : out std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_OUTPUT_6 : out std_logic_vector(CHRONO_COUNTER_N-1 downto 0);
			CHRONO_COUNTER_OUTPUT_7 : out std_logic_vector(CHRONO_COUNTER_N-1 downto 0)
		);
	end component;
	component  COUNTER_MUX
		generic(
			COUNTER_MUX_N : positive := 4
		);
		port(
			COUNTER_MUX_OUTPUT_SELECTION : in std_logic;
			COUNTER_MUX_INPUT_0_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_0_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_1_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_1_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_2_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_2_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_3_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_3_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_4_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_4_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_5_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_5_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_6_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_6_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_7_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_INPUT_7_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_OUTPUT_0 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_OUTPUT_1 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_OUTPUT_2 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_OUTPUT_3 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_OUTPUT_4 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_OUTPUT_5 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_OUTPUT_6 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
			COUNTER_MUX_OUTPUT_7 : out std_logic_vector(COUNTER_MUX_N-1 downto 0)
		);
	end component;
	signal GLOBAL_CHRONO_CLOCK : std_logic;
	signal CHRONO_RESET_0 : std_logic;
	signal CHRONO_RESET_1 : std_logic;
	signal CHRONO_START_PAUSE_0 : std_logic;
	signal CHRONO_START_PAUSE_1 : std_logic;
	signal FORWARD_CHRONO_RESET : std_logic;
	signal FORWARD_CHRONO_START_PAUSE : std_logic;
	signal BACKWARD_CHRONO_RESET : std_logic;
	signal BACKWARD_CHRONO_START_PAUSE : std_logic;
	signal FORWARD_CHRONO_OUTPUT_0 : std_logic_vector(CHRONO_N-1 downto 0);
	signal FORWARD_CHRONO_OUTPUT_1 : std_logic_vector(CHRONO_N-1 downto 0);
	signal FORWARD_CHRONO_OUTPUT_2 : std_logic_vector(CHRONO_N-1 downto 0);
	signal FORWARD_CHRONO_OUTPUT_3 : std_logic_vector(CHRONO_N-1 downto 0);
	signal FORWARD_CHRONO_OUTPUT_4 : std_logic_vector(CHRONO_N-1 downto 0);
	signal FORWARD_CHRONO_OUTPUT_5 : std_logic_vector(CHRONO_N-1 downto 0);
	signal FORWARD_CHRONO_OUTPUT_6 : std_logic_vector(CHRONO_N-1 downto 0);
	signal FORWARD_CHRONO_OUTPUT_7 : std_logic_vector(CHRONO_N-1 downto 0);
	signal BACKWARD_CHRONO_OUTPUT_0 : std_logic_vector(CHRONO_N-1 downto 0);
	signal BACKWARD_CHRONO_OUTPUT_1 : std_logic_vector(CHRONO_N-1 downto 0);
	signal BACKWARD_CHRONO_OUTPUT_2 : std_logic_vector(CHRONO_N-1 downto 0);
	signal BACKWARD_CHRONO_OUTPUT_3 : std_logic_vector(CHRONO_N-1 downto 0);
	signal BACKWARD_CHRONO_OUTPUT_4 : std_logic_vector(CHRONO_N-1 downto 0);
	signal BACKWARD_CHRONO_OUTPUT_5 : std_logic_vector(CHRONO_N-1 downto 0);
	signal BACKWARD_CHRONO_OUTPUT_6 : std_logic_vector(CHRONO_N-1 downto 0);
	signal BACKWARD_CHRONO_OUTPUT_7 : std_logic_vector(CHRONO_N-1 downto 0);
begin
	CHRONO_CLOCK_DIVIDER : CLOCK_DIVIDER
		generic map(
			INPUT_FREQUENCY			=> CHRONO_INPUT_FREQUENCY,
			OUTPUT_FREQUENCY		=> CHRONO_WORKING_FREQUENCY
		)
		port map(
			CLOCK_DIVIDER_CLOCK_IN	=> CHRONO_CLOCK,
			CLOCK_DIVIDER_RESET		=> CHRONO_MASTER_RESET,
			CLOCK_DIVIDER_CLOCK_OUT	=> GLOBAL_CHRONO_CLOCK
		);
	CHRONO_DEMUX : COUNTER_DEMUX
		generic map(
			COUNTER_DEMUX_N					=> CHRONO_O
		)
		port map(
			COUNTER_DEMUX_OUTPUT_SELECTION	=> CHRONO_MODE,
			COUNTER_DEMUX_INPUT_0(0)		=> CHRONO_RESET,
			COUNTER_DEMUX_INPUT_1(0)		=> CHRONO_START_PAUSE,
			COUNTER_DEMUX_OUTPUT_0_0(0)		=> CHRONO_RESET_0,
			COUNTER_DEMUX_OUTPUT_0_1(0)		=> CHRONO_RESET_1,
			COUNTER_DEMUX_OUTPUT_1_0(0)		=> CHRONO_START_PAUSE_0,
			COUNTER_DEMUX_OUTPUT_1_1(0)		=> CHRONO_START_PAUSE_1
		);
	FFX0 : FLIP_FLOP_X
		port map(
			FLIP_FLOP_X_CLOCK	=> GLOBAL_CHRONO_CLOCK,
			FLIP_FLOP_X_INPUT	=> CHRONO_START_PAUSE_0,
			FLIP_FLOP_X_RESET	=> FORWARD_CHRONO_RESET,
			FLIP_FLOP_X_OUTPUT	=> FORWARD_CHRONO_START_PAUSE
		);
	FFX1 : FLIP_FLOP_X
		port map(
			FLIP_FLOP_X_CLOCK	=> GLOBAL_CHRONO_CLOCK,
			FLIP_FLOP_X_INPUT	=> CHRONO_START_PAUSE_1,
			FLIP_FLOP_X_RESET	=> BACKWARD_CHRONO_RESET,
			FLIP_FLOP_X_OUTPUT	=> BACKWARD_CHRONO_START_PAUSE
		);
	FORWARD_CHRONO : CHRONO_COUNTER
		generic map(
			CHRONO_COUNTER_N		=> CHRONO_N;
			CHRONO_COUNTER_LIMIT_0	=> CHRONO_LIMIT_0;
			CHRONO_COUNTER_LIMIT_1	=> CHRONO_LIMIT_1;
			CHRONO_COUNTER_LIMIT_2	=> CHRONO_LIMIT_2;
			CHRONO_COUNTER_LIMIT_3	=> CHRONO_LIMIT_3;
			CHRONO_COUNTER_LIMIT_4	=> CHRONO_LIMIT_4;
			CHRONO_COUNTER_LIMIT_5	=> CHRONO_LIMIT_5;
			CHRONO_COUNTER_LIMIT_6	=> CHRONO_LIMIT_6;
			CHRONO_COUNTER_LIMIT_7	=> CHRONO_LIMIT_7
		)
		port map(
			CHRONO_COUNTER_MODE		=> CHRONO_0_MODE,
			CHRONO_COUNTER_CLOCK	=> GLOBAL_CHRONO_CLOCK,
			CHRONO_COUNTER_RESET	=> FORWARD_CHRONO_RESET,
			CHRONO_COUNTER_ENABLE	=> FORWARD_CHRONO_START_PAUSE,
			CHRONO_COUNTER_LOAD_0	=> std_logic_vector(to_unsigned(CHRONO_0_LOAD_0, COUNTER_N)),
			CHRONO_COUNTER_LOAD_1	=> std_logic_vector(to_unsigned(CHRONO_0_LOAD_1, COUNTER_N)),
			CHRONO_COUNTER_LOAD_2	=> std_logic_vector(to_unsigned(CHRONO_0_LOAD_2, COUNTER_N)),
			CHRONO_COUNTER_LOAD_3	=> std_logic_vector(to_unsigned(CHRONO_0_LOAD_3, COUNTER_N)),
			CHRONO_COUNTER_LOAD_4	=> std_logic_vector(to_unsigned(CHRONO_0_LOAD_4, COUNTER_N)),
			CHRONO_COUNTER_LOAD_5	=> std_logic_vector(to_unsigned(CHRONO_0_LOAD_5, COUNTER_N)),
			CHRONO_COUNTER_LOAD_6	=> std_logic_vector(to_unsigned(CHRONO_0_LOAD_6, COUNTER_N)),
			CHRONO_COUNTER_LOAD_7	=> std_logic_vector(to_unsigned(CHRONO_0_LOAD_7, COUNTER_N)),
			CHRONO_COUNTER_OUTPUT_0	=> FORWARD_CHRONO_OUTPUT_0,
			CHRONO_COUNTER_OUTPUT_1	=> FORWARD_CHRONO_OUTPUT_1,
			CHRONO_COUNTER_OUTPUT_2	=> FORWARD_CHRONO_OUTPUT_2,
			CHRONO_COUNTER_OUTPUT_3	=> FORWARD_CHRONO_OUTPUT_3,
			CHRONO_COUNTER_OUTPUT_4	=> FORWARD_CHRONO_OUTPUT_4,
			CHRONO_COUNTER_OUTPUT_5	=> FORWARD_CHRONO_OUTPUT_5,
			CHRONO_COUNTER_OUTPUT_6	=> FORWARD_CHRONO_OUTPUT_6,
			CHRONO_COUNTER_OUTPUT_7	=> FORWARD_CHRONO_OUTPUT_7
		);
	BACKWARD_CHRONO : CHRONO_COUNTER
		generic map(
			CHRONO_COUNTER_N		=> CHRONO_N;
			CHRONO_COUNTER_LIMIT_0	=> CHRONO_LIMIT_0;
			CHRONO_COUNTER_LIMIT_1	=> CHRONO_LIMIT_1;
			CHRONO_COUNTER_LIMIT_2	=> CHRONO_LIMIT_2;
			CHRONO_COUNTER_LIMIT_3	=> CHRONO_LIMIT_3;
			CHRONO_COUNTER_LIMIT_4	=> CHRONO_LIMIT_4;
			CHRONO_COUNTER_LIMIT_5	=> CHRONO_LIMIT_5;
			CHRONO_COUNTER_LIMIT_6	=> CHRONO_LIMIT_6;
			CHRONO_COUNTER_LIMIT_7	=> CHRONO_LIMIT_7
		)
		port map(
			CHRONO_COUNTER_MODE		=> CHRONO_1_MODE,
			CHRONO_COUNTER_CLOCK	=> GLOBAL_CHRONO_CLOCK,
			CHRONO_COUNTER_RESET	=> BACKWARD_CHRONO_RESET,
			CHRONO_COUNTER_ENABLE	=> BACKWARD_CHRONO_START_PAUSE,
			CHRONO_COUNTER_LOAD_0	=> std_logic_vector(to_unsigned(CHRONO_1_LOAD_0, COUNTER_N)),
			CHRONO_COUNTER_LOAD_1	=> std_logic_vector(to_unsigned(CHRONO_1_LOAD_1, COUNTER_N)),
			CHRONO_COUNTER_LOAD_2	=> std_logic_vector(to_unsigned(CHRONO_1_LOAD_2, COUNTER_N)),
			CHRONO_COUNTER_LOAD_3	=> std_logic_vector(to_unsigned(CHRONO_1_LOAD_3, COUNTER_N)),
			CHRONO_COUNTER_LOAD_4	=> std_logic_vector(to_unsigned(CHRONO_1_LOAD_4, COUNTER_N)),
			CHRONO_COUNTER_LOAD_5	=> std_logic_vector(to_unsigned(CHRONO_1_LOAD_5, COUNTER_N)),
			CHRONO_COUNTER_LOAD_6	=> std_logic_vector(to_unsigned(CHRONO_1_LOAD_6, COUNTER_N)),
			CHRONO_COUNTER_LOAD_7	=> std_logic_vector(to_unsigned(CHRONO_1_LOAD_7, COUNTER_N)),
			CHRONO_COUNTER_OUTPUT_0	=> BACKWARD_CHRONO_OUTPUT_0,
			CHRONO_COUNTER_OUTPUT_1	=> BACKWARD_CHRONO_OUTPUT_1,
			CHRONO_COUNTER_OUTPUT_2	=> BACKWARD_CHRONO_OUTPUT_2,
			CHRONO_COUNTER_OUTPUT_3	=> BACKWARD_CHRONO_OUTPUT_3,
			CHRONO_COUNTER_OUTPUT_4	=> BACKWARD_CHRONO_OUTPUT_4,
			CHRONO_COUNTER_OUTPUT_5	=> BACKWARD_CHRONO_OUTPUT_5,
			CHRONO_COUNTER_OUTPUT_6	=> BACKWARD_CHRONO_OUTPUT_6,
			CHRONO_COUNTER_OUTPUT_7	=> BACKWARD_CHRONO_OUTPUT_7
		);
	CHRONO_MUX : COUNTER_MUX
		generic map(
			COUNTER_MUX_N					=> CHRONO_N
		)
		port map(
			COUNTER_MUX_OUTPUT_SELECTION	=> CHRONO_MODE,
			COUNTER_MUX_INPUT_0_0			=> FORWARD_CHRONO_OUTPUT_0,
			COUNTER_MUX_INPUT_0_1			=> BACKWARD_CHRONO_OUTPUT_0,
			COUNTER_MUX_INPUT_1_0			=> FORWARD_CHRONO_OUTPUT_1,
			COUNTER_MUX_INPUT_1_1			=> BACKWARD_CHRONO_OUTPUT_1,
			COUNTER_MUX_INPUT_2_0			=> FORWARD_CHRONO_OUTPUT_2,
			COUNTER_MUX_INPUT_2_1			=> BACKWARD_CHRONO_OUTPUT_2,
			COUNTER_MUX_INPUT_3_0			=> FORWARD_CHRONO_OUTPUT_3,
			COUNTER_MUX_INPUT_3_1			=> BACKWARD_CHRONO_OUTPUT_3,
			COUNTER_MUX_INPUT_4_0			=> FORWARD_CHRONO_OUTPUT_4,
			COUNTER_MUX_INPUT_4_1			=> BACKWARD_CHRONO_OUTPUT_4,
			COUNTER_MUX_INPUT_5_0			=> FORWARD_CHRONO_OUTPUT_5,
			COUNTER_MUX_INPUT_5_1			=> BACKWARD_CHRONO_OUTPUT_5,
			COUNTER_MUX_INPUT_6_0			=> FORWARD_CHRONO_OUTPUT_6,
			COUNTER_MUX_INPUT_6_1			=> BACKWARD_CHRONO_OUTPUT_6,
			COUNTER_MUX_INPUT_7_0			=> FORWARD_CHRONO_OUTPUT_7,
			COUNTER_MUX_INPUT_7_1			=> BACKWARD_CHRONO_OUTPUT_7,
			COUNTER_MUX_OUTPUT_0			=> CHRONO_OUTPUT_0,
			COUNTER_MUX_OUTPUT_1			=> CHRONO_OUTPUT_1,
			COUNTER_MUX_OUTPUT_2			=> CHRONO_OUTPUT_2,
			COUNTER_MUX_OUTPUT_3			=> CHRONO_OUTPUT_3,
			COUNTER_MUX_OUTPUT_4			=> CHRONO_OUTPUT_4,
			COUNTER_MUX_OUTPUT_5			=> CHRONO_OUTPUT_5,
			COUNTER_MUX_OUTPUT_6			=> CHRONO_OUTPUT_6,
			COUNTER_MUX_OUTPUT_7			=> CHRONO_OUTPUT_7
		);
	FORWARD_CHRONO_RESET	<= CHRONO_MASTER_RESET OR CHRONO_RESET_0;
	BACKWARD_CHRONO_RESET	<= CHRONO_MASTER_RESET OR CHRONO_RESET_1;
end architecture STRUCTURAL;