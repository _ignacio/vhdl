----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 09.01.2020
-- Design Name:
-- Module Name: DECODER - BEHAVIORAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 14.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DECODER is
	generic(
		DECODER_N : positive := 4;
		DECODER_M : positive := 7
	);
	port(
		DECODER_INPUT : in std_logic_vector(DECODER_N-1 downto 0);
		DECODER_OUTPUT : out std_logic_vector(DECODER_M-1 downto 0)
	);
end entity DECODER;

architecture BEHAVIORAL of DECODER is
begin
	process
		variable V_COUNTER : integer;
	begin
		V_COUNTER := to_integer(unsigned(DECODER_INPUT));
		case V_COUNTER is
			when 0	=> DECODER_OUTPUT <= "1000000";--0
			when 1	=> DECODER_OUTPUT <= "1111001";--1
			when 2	=> DECODER_OUTPUT <= "0100100";--2
			when 3	=> DECODER_OUTPUT <= "0110000";--3
			when 4	=> DECODER_OUTPUT <= "0011001";--4
			when 5	=> DECODER_OUTPUT <= "0010010";--5
			when 6	=> DECODER_OUTPUT <= "0000010";--6
			when 7	=> DECODER_OUTPUT <= "1111000";--7
			when 8	=> DECODER_OUTPUT <= "0000000";--8
			when 9	=> DECODER_OUTPUT <= "0011000";--9
			when 10	=> DECODER_OUTPUT <= "0001000";--A
			when 11	=> DECODER_OUTPUT <= "0000011";--b
			when 12	=> DECODER_OUTPUT <= "0100111";--c
			when 13	=> DECODER_OUTPUT <= "0100001";--d
			when 14	=> DECODER_OUTPUT <= "0000110";--E
			when 15	=> DECODER_OUTPUT <= "0001110";--F
			when others => DECODER_OUTPUT <= "1000000";
		end case;
	end process;
end architecture BEHAVIORAL;