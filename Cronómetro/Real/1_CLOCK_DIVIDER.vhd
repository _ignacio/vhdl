----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO 
-- 
-- Create Date: 03.01.2020
-- Design Name:
-- Module Name: CLOCK_DIVIDER - BEHAVIORAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 14.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity CLOCK_DIVIDER is
	generic(
		INPUT_FREQUENCY : positive := 100000000;
		OUTPUT_FREQUENCY : positive := 10000
	);
	port(
		CLOCK_DIVIDER_CLOCK_IN : in std_logic;
		CLOCK_DIVIDER_RESET : in std_logic;
		CLOCK_DIVIDER_CLOCK_OUT : out std_logic
	);
end entity CLOCK_DIVIDER;

architecture BEHAVIORAL of CLOCK_DIVIDER is
	signal CLOCK_SIGNAL : std_logic;
begin
	process(CLOCK_DIVIDER_RESET, CLOCK_DIVIDER_CLOCK_IN)
		variable COUNTER : integer;
	begin
		if(CLOCK_DIVIDER_RESET = '1') then
			CLOCK_SIGNAL <= '0';
			COUNTER := 0;
		elsif(rising_edge(CLOCK_DIVIDER_CLOCK_IN)) then
			if(COUNTER = INPUT_FREQUENCY/(2*OUTPUT_FREQUENCY)-1) then 
				CLOCK_SIGNAL <= not(CLOCK_SIGNAL);
				COUNTER := 0;
			else
				COUNTER := COUNTER+1;
			end if;
		end if;
	end process;
	CLOCK_DIVIDER_CLOCK_OUT <= CLOCK_SIGNAL;
end architecture BEHAVIORAL;