----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 09.01.2020
-- Design Name:
-- Module Name: COUNTER_MUX - STRUCTURAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 14.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity COUNTER_MUX is
	generic(
		COUNTER_MUX_N : positive := 4
	);
	port(
		COUNTER_MUX_OUTPUT_SELECTION : in std_logic;
		COUNTER_MUX_INPUT_0_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_0_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_1_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_1_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_2_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_2_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_3_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_3_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_4_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_4_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_5_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_5_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_6_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_6_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_7_0 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_INPUT_7_1 : in std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_OUTPUT_0 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_OUTPUT_1 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_OUTPUT_2 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_OUTPUT_3 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_OUTPUT_4 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_OUTPUT_5 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_OUTPUT_6 : out std_logic_vector(COUNTER_MUX_N-1 downto 0);
		COUNTER_MUX_OUTPUT_7 : out std_logic_vector(COUNTER_MUX_N-1 downto 0)
	);
end entity COUNTER_MUX;

architecture STRUCTURAL of COUNTER_MUX is
	component  MUX
		generic(
			MUX_N : positive := 4
		);
		port(
			MUX_INPUT_SELECTION : in std_logic;
			MUX_INPUT_0 : in std_logic_vector(MUX_N-1 downto 0);
			MUX_INPUT_1 : in std_logic_vector(MUX_N-1 downto 0);
			MUX_OUTPUT : out std_logic_vector(MUX_N-1 downto 0)
		);
	end component;
begin
	MUX_0 : MUX
		generic map(
			MUX_N				=> COUNTER_MUX_N
		)
		port map(
			MUX_INPUT_SELECTION	=> COUNTER_MUX_OUTPUT_SELECTION,
			MUX_INPUT_0			=> COUNTER_MUX_INPUT_0_0,
			MUX_INPUT_1			=> COUNTER_MUX_INPUT_0_1,
			MUX_OUTPUT			=> COUNTER_MUX_OUTPUT_0
		);
	MUX_1 : MUX
		generic map(
			MUX_N				=> COUNTER_MUX_N
		)
		port map(
			MUX_INPUT_SELECTION	=> COUNTER_MUX_OUTPUT_SELECTION,
			MUX_INPUT_0			=> COUNTER_MUX_INPUT_1_0,
			MUX_INPUT_1			=> COUNTER_MUX_INPUT_1_1,
			MUX_OUTPUT			=> COUNTER_MUX_OUTPUT_1
		);
	MUX_2 : MUX
		generic map(
			MUX_N				=> COUNTER_MUX_N
		)
		port map(
			MUX_INPUT_SELECTION	=> COUNTER_MUX_OUTPUT_SELECTION,
			MUX_INPUT_0			=> COUNTER_MUX_INPUT_2_0,
			MUX_INPUT_1			=> COUNTER_MUX_INPUT_2_1,
			MUX_OUTPUT			=> COUNTER_MUX_OUTPUT_2
		);
	MUX_3 : MUX
		generic map(
			MUX_N				=> COUNTER_MUX_N
		)
		port map(
			MUX_INPUT_SELECTION	=> COUNTER_MUX_OUTPUT_SELECTION,
			MUX_INPUT_0			=> COUNTER_MUX_INPUT_3_0,
			MUX_INPUT_1			=> COUNTER_MUX_INPUT_3_1,
			MUX_OUTPUT			=> COUNTER_MUX_OUTPUT_3
		);
	MUX_4 : MUX
		generic map(
			MUX_N				=> COUNTER_MUX_N
		)
		port map(
			MUX_INPUT_SELECTION	=> COUNTER_MUX_OUTPUT_SELECTION,
			MUX_INPUT_0			=> COUNTER_MUX_INPUT_4_0,
			MUX_INPUT_1			=> COUNTER_MUX_INPUT_4_1,
			MUX_OUTPUT			=> COUNTER_MUX_OUTPUT_4
		);
	MUX_5 : MUX
		generic map(
			MUX_N				=> COUNTER_MUX_N
		)
		port map(
			MUX_INPUT_SELECTION	=> COUNTER_MUX_OUTPUT_SELECTION,
			MUX_INPUT_0			=> COUNTER_MUX_INPUT_5_0,
			MUX_INPUT_1			=> COUNTER_MUX_INPUT_5_1,
			MUX_OUTPUT			=> COUNTER_MUX_OUTPUT_5
		);
	MUX_6 : MUX
		generic map(
			MUX_N				=> COUNTER_MUX_N
		)
		port map(
			MUX_INPUT_SELECTION	=> COUNTER_MUX_OUTPUT_SELECTION,
			MUX_INPUT_0			=> COUNTER_MUX_INPUT_6_0,
			MUX_INPUT_1			=> COUNTER_MUX_INPUT_6_1,
			MUX_OUTPUT			=> COUNTER_MUX_OUTPUT_6
		);
	MUX_7 : MUX
		generic map(
			MUX_N				=> COUNTER_MUX_N
		)
		port map(
			MUX_INPUT_SELECTION	=> COUNTER_MUX_OUTPUT_SELECTION,
			MUX_INPUT_0			=> COUNTER_MUX_INPUT_7_0,
			MUX_INPUT_1			=> COUNTER_MUX_INPUT_7_1,
			MUX_OUTPUT			=> COUNTER_MUX_OUTPUT_7
		);
end architecture STRUCTURAL;