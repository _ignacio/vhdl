----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 10.01.2020
-- Design Name:
-- Module Name: FLIP_FLOP_X - BEHAVIORAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: FLIP-FLOP type T
-- 
-- Dependencies: 
-- 
-- Revision: 14.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity FLIP_FLOP_X is
	port(
		FLIP_FLOP_X_CLOCK : in std_logic;
		FLIP_FLOP_X_INPUT : in std_logic;
		FLIP_FLOP_X_RESET : in std_logic;
		FLIP_FLOP_X_OUTPUT : out std_logic
	);
end entity FLIP_FLOP_X;

architecture BEHAVIORAL of FLIP_FLOP_X is
	signal OUTPUT_SIGNAL : std_logic;
	signal PREVIOUS_INPUT_SIGNAL : std_logic;
begin
	process(FLIP_FLOP_X_CLOCK, FLIP_FLOP_X_RESET, FLIP_FLOP_X_INPUT)
	begin
		if(FLIP_FLOP_X_RESET = '1') then
			OUTPUT_SIGNAL <= '0';
			PREVIOUS_INPUT_SIGNAL <= '0';
		elsif(rising_edge(FLIP_FLOP_X_CLOCK)) then
			if(FLIP_FLOP_X_INPUT='0') then
				OUTPUT_SIGNAL <= OUTPUT_SIGNAL;
			elsif(FLIP_FLOP_X_INPUT='1' and PREVIOUS_INPUT_SIGNAL='0') then
				OUTPUT_SIGNAL <= not(OUTPUT_SIGNAL);
			end if;
			PREVIOUS_INPUT_SIGNAL <= FLIP_FLOP_X_INPUT;
		end if;
	end process;
	FLIP_FLOP_X_OUTPUT <= OUTPUT_SIGNAL;
end architecture BEHAVIORAL;