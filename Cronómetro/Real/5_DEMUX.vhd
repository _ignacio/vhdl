----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 03.01.2020
-- Design Name:
-- Module Name: DEMUX - BEHAVIORAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 14.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity DEMUX is
	generic(
		DEMUX_N : positive := 1
	);
	port(
		DEMUX_OUTPUT_SELECTION : in std_logic;
		DEMUX_INPUT : in std_logic_vector(DEMUX_N-1 downto 0);
		DEMUX_OUTPUT_0 : out std_logic_vector(DEMUX_N-1 downto 0);
		DEMUX_OUTPUT_1 : out std_logic_vector(DEMUX_N-1 downto 0)
	);
end entity DEMUX;

architecture BEHAVIORAL of DEMUX is
begin
	process(DEMUX_OUTPUT_SELECTION)
	begin
        case DEMUX_OUTPUT_SELECTION is
			when '0'	=> DEMUX_OUTPUT_0 <=	DEMUX_INPUT;--DEMUX_OUTPUT_SELECTION==0
			when others	=> DEMUX_OUTPUT_1 <=	DEMUX_INPUT;--DEMUX_OUTPUT_SELECTION==1
		end case;
	end process;
end architecture BEHAVIORAL;