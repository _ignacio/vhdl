----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 10.01.2020
-- Design Name:
-- Module Name: DISPLAY_CONTROL - STRUCTURAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 16.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity DISPLAY_CONTROL is
	generic(
		DISPLAY_CONTROL_N			: positive := 4;
		DISPLAY_CONTROL_M			: positive := 7;
		DISPLAY_CONTROL_P			: positive := 8;
		DISPLAY_INPUT_FREQUENCY		: positive := 10000;
		DISPLAY_WORKING_FREQUENCY	: positive := 400
	);
	port(
		DISPLAY_CONTROL_CLOCK : in std_logic;
		DISPLAY_CONTROL_RESET : in std_logic;
		DISPLAY_CONTROL_INPUT_0 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
		DISPLAY_CONTROL_INPUT_1 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
		DISPLAY_CONTROL_INPUT_2 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
		DISPLAY_CONTROL_INPUT_3 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
		DISPLAY_CONTROL_INPUT_4 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
		DISPLAY_CONTROL_INPUT_5 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
		DISPLAY_CONTROL_INPUT_6 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
		DISPLAY_CONTROL_INPUT_7 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
		DISPLAY_CONTROL_OUTPUT_DIGIT_DATA : out std_logic_vector(DISPLAY_CONTROL_M-1 downto 0);
		DISPLAY_CONTROL_OUTPUT_DIGIT_POSITION : out std_logic_vector(DISPLAY_CONTROL_P-1 downto 0)
	);
end entity DISPLAY_CONTROL;

architecture STRUCTURAL of DISPLAY_CONTROL is
	component  CLOCK_DIVIDER
		generic(
			INPUT_FREQUENCY : positive := 10000;
			OUTPUT_FREQUENCY : positive := 400
		);
		port(
			CLOCK_DIVIDER_CLOCK_IN : in std_logic;
			CLOCK_DIVIDER_RESET : in std_logic;
			CLOCK_DIVIDER_CLOCK_OUT : out std_logic
		);
	end component;
	component DECODER
		generic(
			DECODER_N : positive := 4;
			DECODER_M : positive := 7
		);
		port(
			DECODER_INPUT : in std_logic_vector(DECODER_N-1 downto 0);
			DECODER_OUTPUT : out std_logic_vector(DECODER_M-1 downto 0)
		);
	end component;
	component DISPLAY_REFRESH
		generic(
			DISPLAY_REFRESH_N : positive := 7;
			DISPLAY_REFRESH_M : positive := 8
		);
		port(
			DISPLAY_REFRESH_CLOCK : in std_logic;
			DISPLAY_REFRESH_DIGIT_0 : in std_logic_vector(DISPLAY_REFRESH_N-1 downto 0);
			DISPLAY_REFRESH_DIGIT_1 : in std_logic_vector(DISPLAY_REFRESH_N-1 downto 0);
			DISPLAY_REFRESH_DIGIT_2 : in std_logic_vector(DISPLAY_REFRESH_N-1 downto 0);
			DISPLAY_REFRESH_DIGIT_3 : in std_logic_vector(DISPLAY_REFRESH_N-1 downto 0);
			DISPLAY_REFRESH_DIGIT_4 : in std_logic_vector(DISPLAY_REFRESH_N-1 downto 0);
			DISPLAY_REFRESH_DIGIT_5 : in std_logic_vector(DISPLAY_REFRESH_N-1 downto 0);
			DISPLAY_REFRESH_DIGIT_6 : in std_logic_vector(DISPLAY_REFRESH_N-1 downto 0);
			DISPLAY_REFRESH_DIGIT_7 : in std_logic_vector(DISPLAY_REFRESH_N-1 downto 0);
			DISPLAY_REFRESH_DISPLAY_DIGIT : out std_logic_vector(DISPLAY_REFRESH_N-1 downto 0);
			DISPLAY_REFRESH_DISPLAY_SELECTION : out std_logic_vector(DISPLAY_REFRESH_M-1 downto 0)
		);
	end component;
	signal DISPLAY_CONTROL_REFRESH_CLOCK : std_logic;
	signal DISPLAY_CONTROL_DIGIT_0 : std_logic_vector(DISPLAY_CONTROL_M-1 downto 0);
	signal DISPLAY_CONTROL_DIGIT_1 : std_logic_vector(DISPLAY_CONTROL_M-1 downto 0);
	signal DISPLAY_CONTROL_DIGIT_2 : std_logic_vector(DISPLAY_CONTROL_M-1 downto 0);
	signal DISPLAY_CONTROL_DIGIT_3 : std_logic_vector(DISPLAY_CONTROL_M-1 downto 0);
	signal DISPLAY_CONTROL_DIGIT_4 : std_logic_vector(DISPLAY_CONTROL_M-1 downto 0);
	signal DISPLAY_CONTROL_DIGIT_5 : std_logic_vector(DISPLAY_CONTROL_M-1 downto 0);
	signal DISPLAY_CONTROL_DIGIT_6 : std_logic_vector(DISPLAY_CONTROL_M-1 downto 0);
	signal DISPLAY_CONTROL_DIGIT_7 : std_logic_vector(DISPLAY_CONTROL_M-1 downto 0);
begin
	REFRESH_CLOCK_DIVIDER : CLOCK_DIVIDER
		generic map(
			INPUT_FREQUENCY			=> DISPLAY_INPUT_FREQUENCY,
			OUTPUT_FREQUENCY		=> DISPLAY_WORKING_FREQUENCY
		)
		port map(
			CLOCK_DIVIDER_CLOCK_IN	=> DISPLAY_CONTROL_CLOCK,
			CLOCK_DIVIDER_RESET		=> DISPLAY_CONTROL_RESET,
			CLOCK_DIVIDER_CLOCK_OUT	=> DISPLAY_CONTROL_REFRESH_CLOCK
		);
	DECODER_0 : DECODER
		generic map(
			DECODER_N		=> DISPLAY_CONTROL_N,
			DECODER_M		=> DISPLAY_CONTROL_M
		)
		port map(
			DECODER_INPUT	=> DISPLAY_CONTROL_INPUT_0,
			DECODER_OUTPUT	=> DISPLAY_CONTROL_DIGIT_0
		);
	DECODER_1 : DECODER
		generic map(
			DECODER_N		=> DISPLAY_CONTROL_N,
			DECODER_M		=> DISPLAY_CONTROL_M
		)
		port map(
			DECODER_INPUT	=> DISPLAY_CONTROL_INPUT_1,
			DECODER_OUTPUT	=> DISPLAY_CONTROL_DIGIT_1
		);
	DECODER_2 : DECODER
		generic map(
			DECODER_N		=> DISPLAY_CONTROL_N,
			DECODER_M		=> DISPLAY_CONTROL_M
		)
		port map(
			DECODER_INPUT	=> DISPLAY_CONTROL_INPUT_2,
			DECODER_OUTPUT	=> DISPLAY_CONTROL_DIGIT_2
		);
	DECODER_3 : DECODER
		generic map(
			DECODER_N		=> DISPLAY_CONTROL_N,
			DECODER_M		=> DISPLAY_CONTROL_M
		)
		port map(
			DECODER_INPUT	=> DISPLAY_CONTROL_INPUT_3,
			DECODER_OUTPUT	=> DISPLAY_CONTROL_DIGIT_3
		);
	DECODER_4 : DECODER
		generic map(
			DECODER_N		=> DISPLAY_CONTROL_N,
			DECODER_M		=> DISPLAY_CONTROL_M
		)
		port map(
			DECODER_INPUT	=> DISPLAY_CONTROL_INPUT_4,
			DECODER_OUTPUT	=> DISPLAY_CONTROL_DIGIT_4
		);
	DECODER_5 : DECODER
		generic map(
			DECODER_N		=> DISPLAY_CONTROL_N,
			DECODER_M		=> DISPLAY_CONTROL_M
		)
		port map(
			DECODER_INPUT	=> DISPLAY_CONTROL_INPUT_5,
			DECODER_OUTPUT	=> DISPLAY_CONTROL_DIGIT_5
		);
	DECODER_6 : DECODER
		generic map(
			DECODER_N		=> DISPLAY_CONTROL_N,
			DECODER_M		=> DISPLAY_CONTROL_M
		)
		port map(
			DECODER_INPUT	=> DISPLAY_CONTROL_INPUT_6,
			DECODER_OUTPUT	=> DISPLAY_CONTROL_DIGIT_6
		);
	DECODER_7 : DECODER
		generic map(
			DECODER_N		=> DISPLAY_CONTROL_N,
			DECODER_M		=> DISPLAY_CONTROL_M
		)
		port map(
			DECODER_INPUT					=> DISPLAY_CONTROL_INPUT_7,
			DECODER_OUTPUT					=> DISPLAY_CONTROL_DIGIT_7
		);
	CONTROL_DISPLAY_REFRESH : DISPLAY_REFRESH
		generic map(
			DISPLAY_REFRESH_N					=> DISPLAY_CONTROL_M,
			DISPLAY_REFRESH_M					=> DISPLAY_CONTROL_P
		)
		port map(
			DISPLAY_REFRESH_CLOCK				=> DISPLAY_CONTROL_REFRESH_CLOCK,
			DISPLAY_REFRESH_DIGIT_0				=> DISPLAY_CONTROL_DIGIT_0,
			DISPLAY_REFRESH_DIGIT_1				=> DISPLAY_CONTROL_DIGIT_1,
			DISPLAY_REFRESH_DIGIT_2				=> DISPLAY_CONTROL_DIGIT_2,
			DISPLAY_REFRESH_DIGIT_3				=> DISPLAY_CONTROL_DIGIT_3,
			DISPLAY_REFRESH_DIGIT_4				=> DISPLAY_CONTROL_DIGIT_4,
			DISPLAY_REFRESH_DIGIT_5				=> DISPLAY_CONTROL_DIGIT_5,
			DISPLAY_REFRESH_DIGIT_6				=> DISPLAY_CONTROL_DIGIT_6,
			DISPLAY_REFRESH_DIGIT_7				=> DISPLAY_CONTROL_DIGIT_7,
			DISPLAY_REFRESH_DISPLAY_DIGIT		=> DISPLAY_CONTROL_OUTPUT_DIGIT_DATA,
			DISPLAY_REFRESH_DISPLAY_SELECTION	=> DISPLAY_CONTROL_OUTPUT_DIGIT_POSITION
		);
end architecture STRUCTURAL;