
q
Command: %s
53*	vivadotcl2@
,synth_design -top TOP -part xc7a100tcsg324-12default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7a100t2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7a100t2default:defaultZ17-349h px� 
W
Loading part %s157*device2$
xc7a100tcsg324-12default:defaultZ21-403h px� 
�
%s*synth2�
xStarting RTL Elaboration : Time (s): cpu = 00:00:03 ; elapsed = 00:00:04 . Memory (MB): peak = 807.410 ; gain = 234.613
2default:defaulth px� 
�
synthesizing module '%s'638*oasys2
TOP2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/16_TOP.vhd2default:default2
862default:default8@Z8-638h px� 
Z
%s
*synth2B
.	Parameter TOP_O bound to: 1 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter TOP_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter TOP_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter TOP_P bound to: 8 - type: integer 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter TOP_CONDITIONER_SYNCHRONIZER_INDEX bound to: 2 - type: integer 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter TOP_CONDITIONER_DEBOUNCER_DELAY bound to: 1000 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter TOP_INPUT_FREQUENCY bound to: 100000000 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter TOP_GLOBAL_FREQUENCY bound to: 100000 - type: integer 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter TOP_CHRONO_FREQUENCY bound to: 10000 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter TOP_DISPLAY_FREQUENCY bound to: 400 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_0_LIMIT_0 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_0_LIMIT_1 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_0_LIMIT_2 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_0_LIMIT_3 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_0_LIMIT_4 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_0_LIMIT_5 bound to: 5 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_0_LIMIT_6 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_0_LIMIT_7 bound to: 5 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter TOP_CHRONO_0_MODE bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_0_LOAD_0 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_0_LOAD_1 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_0_LOAD_2 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_0_LOAD_3 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_0_LOAD_4 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_0_LOAD_5 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_0_LOAD_6 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_0_LOAD_7 bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_1_LIMIT_0 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_1_LIMIT_1 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_1_LIMIT_2 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_1_LIMIT_3 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_1_LIMIT_4 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_1_LIMIT_5 bound to: 5 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_1_LIMIT_6 bound to: 9 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter TOP_CHRONO_1_LIMIT_7 bound to: 5 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter TOP_CHRONO_1_MODE bound to: 1'b1 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_1_LOAD_0 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_1_LOAD_1 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_1_LOAD_2 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_1_LOAD_3 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_1_LOAD_4 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_1_LOAD_5 bound to: 3 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_1_LOAD_6 bound to: 0 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter TOP_CHRONO_1_LOAD_7 bound to: 0 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter INPUT_FREQUENCY bound to: 100000000 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter OUTPUT_FREQUENCY bound to: 100000 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2!
CLOCK_DIVIDER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/1_CLOCK_DIVIDER.vhd2default:default2
232default:default2%
TOP_CLOCK_DIVIDER2default:default2!
CLOCK_DIVIDER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/16_TOP.vhd2default:default2
2102default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2!
CLOCK_DIVIDER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/1_CLOCK_DIVIDER.vhd2default:default2
352default:default8@Z8-638h px� 
l
%s
*synth2T
@	Parameter INPUT_FREQUENCY bound to: 100000000 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter OUTPUT_FREQUENCY bound to: 100000 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2!
CLOCK_DIVIDER2default:default2
12default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/1_CLOCK_DIVIDER.vhd2default:default2
352default:default8@Z8-256h px� 
b
%s
*synth2J
6	Parameter CONDITIONER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter CONDITIONER_SYNCHRONIZER_INDEX bound to: 2 - type: integer 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter CONDITIONER_DEBOUNCER_DELAY bound to: 1000 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
CONDITIONER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/15_CONDITIONER.vhd2default:default2
232default:default2#
TOP_CONDITIONER2default:default2
CONDITIONER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/16_TOP.vhd2default:default2
2202default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
CONDITIONER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/15_CONDITIONER.vhd2default:default2
422default:default8@Z8-638h px� 
b
%s
*synth2J
6	Parameter CONDITIONER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter CONDITIONER_SYNCHRONIZER_INDEX bound to: 2 - type: integer 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter CONDITIONER_DEBOUNCER_DELAY bound to: 1000 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter SYNCHRONIZER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SYNCHRONIZER_INDEX bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2 
SYNCHRONIZER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/7_SYNCHRONIZER.vhd2default:default2
232default:default2%
SYNCHRONIZER_MODE2default:default2 
SYNCHRONIZER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/15_CONDITIONER.vhd2default:default2
772default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2 
SYNCHRONIZER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/7_SYNCHRONIZER.vhd2default:default2
362default:default8@Z8-638h px� 
c
%s
*synth2K
7	Parameter SYNCHRONIZER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SYNCHRONIZER_INDEX bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2 
SYNCHRONIZER2default:default2
22default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/7_SYNCHRONIZER.vhd2default:default2
362default:default8@Z8-256h px� 
c
%s
*synth2K
7	Parameter SYNCHRONIZER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SYNCHRONIZER_INDEX bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2 
SYNCHRONIZER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/7_SYNCHRONIZER.vhd2default:default2
232default:default2&
SYNCHRONIZER_RESET2default:default2 
SYNCHRONIZER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/15_CONDITIONER.vhd2default:default2
882default:default8@Z8-3491h px� 
c
%s
*synth2K
7	Parameter SYNCHRONIZER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SYNCHRONIZER_INDEX bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2 
SYNCHRONIZER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/7_SYNCHRONIZER.vhd2default:default2
232default:default2,
SYNCHRONIZER_START_PAUSE2default:default2 
SYNCHRONIZER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/15_CONDITIONER.vhd2default:default2
992default:default8@Z8-3491h px� 
c
%s
*synth2K
7	Parameter SYNCHRONIZER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SYNCHRONIZER_INDEX bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2 
SYNCHRONIZER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/7_SYNCHRONIZER.vhd2default:default2
232default:default2-
SYNCHRONIZER_MASTER_RESET2default:default2 
SYNCHRONIZER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/15_CONDITIONER.vhd2default:default2
1102default:default8@Z8-3491h px� 
`
%s
*synth2H
4	Parameter DEBOUNCER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DEBOUNCER_DELAY bound to: 1000 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
	DEBOUNCER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/8_DEBOUNCER.vhd2default:default2
242default:default2"
DEBOUNCER_MODE2default:default2
	DEBOUNCER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/15_CONDITIONER.vhd2default:default2
1222default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
	DEBOUNCER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/8_DEBOUNCER.vhd2default:default2
372default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter DEBOUNCER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DEBOUNCER_DELAY bound to: 1000 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
	DEBOUNCER2default:default2
32default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/8_DEBOUNCER.vhd2default:default2
372default:default8@Z8-256h px� 
`
%s
*synth2H
4	Parameter DEBOUNCER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DEBOUNCER_DELAY bound to: 1000 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
	DEBOUNCER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/8_DEBOUNCER.vhd2default:default2
242default:default2#
DEBOUNCER_RESET2default:default2
	DEBOUNCER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/15_CONDITIONER.vhd2default:default2
1332default:default8@Z8-3491h px� 
`
%s
*synth2H
4	Parameter DEBOUNCER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DEBOUNCER_DELAY bound to: 1000 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
	DEBOUNCER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/8_DEBOUNCER.vhd2default:default2
242default:default2)
DEBOUNCER_START_PAUSE2default:default2
	DEBOUNCER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/15_CONDITIONER.vhd2default:default2
1442default:default8@Z8-3491h px� 
`
%s
*synth2H
4	Parameter DEBOUNCER_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DEBOUNCER_DELAY bound to: 1000 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
	DEBOUNCER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/8_DEBOUNCER.vhd2default:default2
242default:default2*
DEBOUNCER_MASTER_RESET2default:default2
	DEBOUNCER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/15_CONDITIONER.vhd2default:default2
1552default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
CONDITIONER2default:default2
42default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/15_CONDITIONER.vhd2default:default2
422default:default8@Z8-256h px� 
]
%s
*synth2E
1	Parameter CHRONO_O bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter CHRONO_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter CHRONO_INPUT_FREQUENCY bound to: 100000 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter CHRONO_WORKING_FREQUENCY bound to: 10000 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_0 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_1 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_2 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_3 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_4 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_5 bound to: 5 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_6 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_7 bound to: 5 - type: integer 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter CHRONO_0_MODE bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_0 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_1 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_2 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_3 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_4 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_5 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_6 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_7 bound to: 0 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_0 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_1 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_2 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_3 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_4 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_5 bound to: 5 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_6 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_7 bound to: 5 - type: integer 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter CHRONO_1_MODE bound to: 1'b1 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_0 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_1 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_2 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_3 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_4 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_5 bound to: 3 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_6 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_7 bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
CHRONO2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/14_CHRONO.vhd2default:default2
242default:default2

TOP_CHRONO2default:default2
CHRONO2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/16_TOP.vhd2default:default2
2372default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
CHRONO2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/14_CHRONO.vhd2default:default2
822default:default8@Z8-638h px� 
]
%s
*synth2E
1	Parameter CHRONO_O bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter CHRONO_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter CHRONO_INPUT_FREQUENCY bound to: 100000 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter CHRONO_WORKING_FREQUENCY bound to: 10000 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_0 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_1 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_2 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_3 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_4 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_5 bound to: 5 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_6 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_0_LIMIT_7 bound to: 5 - type: integer 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter CHRONO_0_MODE bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_0 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_1 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_2 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_3 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_4 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_5 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_6 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_0_LOAD_7 bound to: 0 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_0 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_1 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_2 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_3 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_4 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_5 bound to: 5 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_6 bound to: 9 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHRONO_1_LIMIT_7 bound to: 5 - type: integer 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter CHRONO_1_MODE bound to: 1'b1 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_0 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_1 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_2 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_3 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_4 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_5 bound to: 3 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_6 bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CHRONO_1_LOAD_7 bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter INPUT_FREQUENCY bound to: 100000 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter OUTPUT_FREQUENCY bound to: 10000 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2!
CLOCK_DIVIDER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/1_CLOCK_DIVIDER.vhd2default:default2
232default:default2(
CHRONO_CLOCK_DIVIDER2default:default2!
CLOCK_DIVIDER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/14_CHRONO.vhd2default:default2
1992default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys21
CLOCK_DIVIDER__parameterized12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/1_CLOCK_DIVIDER.vhd2default:default2
352default:default8@Z8-638h px� 
i
%s
*synth2Q
=	Parameter INPUT_FREQUENCY bound to: 100000 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter OUTPUT_FREQUENCY bound to: 10000 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys21
CLOCK_DIVIDER__parameterized12default:default2
42default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/1_CLOCK_DIVIDER.vhd2default:default2
352default:default8@Z8-256h px� 
d
%s
*synth2L
8	Parameter COUNTER_DEMUX_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2!
COUNTER_DEMUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/10_COUNTER_DEMUX.vhd2default:default2
232default:default2 
CHRONO_DEMUX2default:default2!
COUNTER_DEMUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/14_CHRONO.vhd2default:default2
2092default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2!
COUNTER_DEMUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/10_COUNTER_DEMUX.vhd2default:default2
382default:default8@Z8-638h px� 
d
%s
*synth2L
8	Parameter COUNTER_DEMUX_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter DEMUX_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
DEMUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/5_DEMUX.vhd2default:default2
232default:default2
DEMUX_02default:default2
DEMUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/10_COUNTER_DEMUX.vhd2default:default2
512default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
DEMUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/5_DEMUX.vhd2default:default2
352default:default8@Z8-638h px� 
\
%s
*synth2D
0	Parameter DEMUX_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2
DEMUX_INPUT2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/5_DEMUX.vhd2default:default2
372default:default8@Z8-614h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
DEMUX2default:default2
52default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/5_DEMUX.vhd2default:default2
352default:default8@Z8-256h px� 
\
%s
*synth2D
0	Parameter DEMUX_N bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
DEMUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/5_DEMUX.vhd2default:default2
232default:default2
DEMUX_12default:default2
DEMUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/10_COUNTER_DEMUX.vhd2default:default2
612default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2!
COUNTER_DEMUX2default:default2
62default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/10_COUNTER_DEMUX.vhd2default:default2
382default:default8@Z8-256h px� 
e
%s
*synth2M
9	Parameter CHRONO_COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_0 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_1 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_2 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_3 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_4 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_5 bound to: 5 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_6 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_7 bound to: 5 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2"
CHRONO_COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
232default:default2
CHRONO_02default:default2"
CHRONO_COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/14_CHRONO.vhd2default:default2
2222default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2"
CHRONO_COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
592default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter CHRONO_COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_0 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_1 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_2 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_3 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_4 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_5 bound to: 5 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_6 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_7 bound to: 5 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
FLIP_FLOP_X2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/9_FLIP_FLOP_X.vhd2default:default2
232default:default2
FFX2default:default2
FLIP_FLOP_X2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
942default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
FLIP_FLOP_X2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/9_FLIP_FLOP_X.vhd2default:default2
322default:default8@Z8-638h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
FLIP_FLOP_X2default:default2
72default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/9_FLIP_FLOP_X.vhd2default:default2
322default:default8@Z8-256h px� 
^
%s
*synth2F
2	Parameter COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COUNTER_LIMIT bound to: 9 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
242default:default2
	COUNTER_02default:default2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
1012default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
402default:default8@Z8-638h px� 
^
%s
*synth2F
2	Parameter COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COUNTER_LIMIT bound to: 9 - type: integer 
2default:defaulth p
x
� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2 
COUNTER_LOAD2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
442default:default8@Z8-614h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
COUNTER2default:default2
82default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
402default:default8@Z8-256h px� 
^
%s
*synth2F
2	Parameter COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COUNTER_LIMIT bound to: 9 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
242default:default2
	COUNTER_12default:default2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
1152default:default8@Z8-3491h px� 
^
%s
*synth2F
2	Parameter COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COUNTER_LIMIT bound to: 9 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
242default:default2
	COUNTER_22default:default2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
1292default:default8@Z8-3491h px� 
^
%s
*synth2F
2	Parameter COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COUNTER_LIMIT bound to: 9 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
242default:default2
	COUNTER_32default:default2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
1432default:default8@Z8-3491h px� 
^
%s
*synth2F
2	Parameter COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COUNTER_LIMIT bound to: 9 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
242default:default2
	COUNTER_42default:default2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
1572default:default8@Z8-3491h px� 
^
%s
*synth2F
2	Parameter COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COUNTER_LIMIT bound to: 5 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
242default:default2
	COUNTER_52default:default2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
1712default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2+
COUNTER__parameterized22default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
402default:default8@Z8-638h px� 
^
%s
*synth2F
2	Parameter COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COUNTER_LIMIT bound to: 5 - type: integer 
2default:defaulth p
x
� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2 
COUNTER_LOAD2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
442default:default8@Z8-614h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2+
COUNTER__parameterized22default:default2
82default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
402default:default8@Z8-256h px� 
^
%s
*synth2F
2	Parameter COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COUNTER_LIMIT bound to: 9 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
242default:default2
	COUNTER_62default:default2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
1852default:default8@Z8-3491h px� 
^
%s
*synth2F
2	Parameter COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COUNTER_LIMIT bound to: 5 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/2_COUNTER.vhd2default:default2
242default:default2
	COUNTER_72default:default2
COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
1992default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2"
CHRONO_COUNTER2default:default2
92default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
592default:default8@Z8-256h px� 
e
%s
*synth2M
9	Parameter CHRONO_COUNTER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_0 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_1 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_2 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_3 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_4 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_5 bound to: 5 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_6 bound to: 9 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHRONO_COUNTER_LIMIT_7 bound to: 5 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2"
CHRONO_COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/12_CHRONO_COUNTER.vhd2default:default2
232default:default2
CHRONO_12default:default2"
CHRONO_COUNTER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/14_CHRONO.vhd2default:default2
2562default:default8@Z8-3491h px� 
b
%s
*synth2J
6	Parameter COUNTER_MUX_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
COUNTER_MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/11_COUNTER_MUX.vhd2default:default2
232default:default2

CHRONO_MUX2default:default2
COUNTER_MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/14_CHRONO.vhd2default:default2
2902default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
COUNTER_MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/11_COUNTER_MUX.vhd2default:default2
562default:default8@Z8-638h px� 
b
%s
*synth2J
6	Parameter COUNTER_MUX_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter MUX_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/4_MUX.vhd2default:default2
232default:default2
MUX_02default:default2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/11_COUNTER_MUX.vhd2default:default2
692default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/4_MUX.vhd2default:default2
352default:default8@Z8-638h px� 
Z
%s
*synth2B
.	Parameter MUX_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
MUX2default:default2
102default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/4_MUX.vhd2default:default2
352default:default8@Z8-256h px� 
Z
%s
*synth2B
.	Parameter MUX_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/4_MUX.vhd2default:default2
232default:default2
MUX_12default:default2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/11_COUNTER_MUX.vhd2default:default2
792default:default8@Z8-3491h px� 
Z
%s
*synth2B
.	Parameter MUX_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/4_MUX.vhd2default:default2
232default:default2
MUX_22default:default2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/11_COUNTER_MUX.vhd2default:default2
892default:default8@Z8-3491h px� 
Z
%s
*synth2B
.	Parameter MUX_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/4_MUX.vhd2default:default2
232default:default2
MUX_32default:default2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/11_COUNTER_MUX.vhd2default:default2
992default:default8@Z8-3491h px� 
Z
%s
*synth2B
.	Parameter MUX_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/4_MUX.vhd2default:default2
232default:default2
MUX_42default:default2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/11_COUNTER_MUX.vhd2default:default2
1092default:default8@Z8-3491h px� 
Z
%s
*synth2B
.	Parameter MUX_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/4_MUX.vhd2default:default2
232default:default2
MUX_52default:default2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/11_COUNTER_MUX.vhd2default:default2
1192default:default8@Z8-3491h px� 
Z
%s
*synth2B
.	Parameter MUX_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/4_MUX.vhd2default:default2
232default:default2
MUX_62default:default2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/11_COUNTER_MUX.vhd2default:default2
1292default:default8@Z8-3491h px� 
Z
%s
*synth2B
.	Parameter MUX_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/4_MUX.vhd2default:default2
232default:default2
MUX_72default:default2
MUX2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/11_COUNTER_MUX.vhd2default:default2
1392default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
COUNTER_MUX2default:default2
112default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/11_COUNTER_MUX.vhd2default:default2
562default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
CHRONO2default:default2
122default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/14_CHRONO.vhd2default:default2
822default:default8@Z8-256h px� 
f
%s
*synth2N
:	Parameter DISPLAY_CONTROL_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter DISPLAY_CONTROL_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter DISPLAY_CONTROL_P bound to: 8 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter DISPLAY_INPUT_FREQUENCY bound to: 100000 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter DISPLAY_WORKING_FREQUENCY bound to: 400 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2#
DISPLAY_CONTROL2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
232default:default2'
TOP_DISPLAY_CONTROL2default:default2#
DISPLAY_CONTROL2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/16_TOP.vhd2default:default2
2932default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2#
DISPLAY_CONTROL2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
472default:default8@Z8-638h px� 
f
%s
*synth2N
:	Parameter DISPLAY_CONTROL_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter DISPLAY_CONTROL_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter DISPLAY_CONTROL_P bound to: 8 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter DISPLAY_INPUT_FREQUENCY bound to: 100000 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter DISPLAY_WORKING_FREQUENCY bound to: 400 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter INPUT_FREQUENCY bound to: 100000 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter OUTPUT_FREQUENCY bound to: 400 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2!
CLOCK_DIVIDER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/1_CLOCK_DIVIDER.vhd2default:default2
232default:default2)
REFRESH_CLOCK_DIVIDER2default:default2!
CLOCK_DIVIDER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
982default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys21
CLOCK_DIVIDER__parameterized32default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/1_CLOCK_DIVIDER.vhd2default:default2
352default:default8@Z8-638h px� 
i
%s
*synth2Q
=	Parameter INPUT_FREQUENCY bound to: 100000 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter OUTPUT_FREQUENCY bound to: 400 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys21
CLOCK_DIVIDER__parameterized32default:default2
122default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/1_CLOCK_DIVIDER.vhd2default:default2
352default:default8@Z8-256h px� 
^
%s
*synth2F
2	Parameter DECODER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter DECODER_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/3_DECODER.vhd2default:default2
242default:default2
	DECODER_02default:default2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
1082default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/3_DECODER.vhd2default:default2
352default:default8@Z8-638h px� 
^
%s
*synth2F
2	Parameter DECODER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter DECODER_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
DECODER2default:default2
132default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/3_DECODER.vhd2default:default2
352default:default8@Z8-256h px� 
^
%s
*synth2F
2	Parameter DECODER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter DECODER_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/3_DECODER.vhd2default:default2
242default:default2
	DECODER_12default:default2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
1172default:default8@Z8-3491h px� 
^
%s
*synth2F
2	Parameter DECODER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter DECODER_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/3_DECODER.vhd2default:default2
242default:default2
	DECODER_22default:default2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
1262default:default8@Z8-3491h px� 
^
%s
*synth2F
2	Parameter DECODER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter DECODER_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/3_DECODER.vhd2default:default2
242default:default2
	DECODER_32default:default2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
1352default:default8@Z8-3491h px� 
^
%s
*synth2F
2	Parameter DECODER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter DECODER_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/3_DECODER.vhd2default:default2
242default:default2
	DECODER_42default:default2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
1442default:default8@Z8-3491h px� 
^
%s
*synth2F
2	Parameter DECODER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter DECODER_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/3_DECODER.vhd2default:default2
242default:default2
	DECODER_52default:default2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
1532default:default8@Z8-3491h px� 
^
%s
*synth2F
2	Parameter DECODER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter DECODER_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/3_DECODER.vhd2default:default2
242default:default2
	DECODER_62default:default2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
1622default:default8@Z8-3491h px� 
^
%s
*synth2F
2	Parameter DECODER_N bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter DECODER_M bound to: 7 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/3_DECODER.vhd2default:default2
242default:default2
	DECODER_72default:default2
DECODER2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
1712default:default8@Z8-3491h px� 
f
%s
*synth2N
:	Parameter DISPLAY_REFRESH_N bound to: 7 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter DISPLAY_REFRESH_M bound to: 8 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2#
DISPLAY_REFRESH2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/6_DISPLAY_REFRESH.vhd2default:default2
232default:default2+
CONTROL_DISPLAY_REFRESH2default:default2#
DISPLAY_REFRESH2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
1802default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2#
DISPLAY_REFRESH2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/6_DISPLAY_REFRESH.vhd2default:default2
432default:default8@Z8-638h px� 
f
%s
*synth2N
:	Parameter DISPLAY_REFRESH_N bound to: 7 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter DISPLAY_REFRESH_M bound to: 8 - type: integer 
2default:defaulth p
x
� 
�
default block is never used226*oasys2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/6_DISPLAY_REFRESH.vhd2default:default2
552default:default8@Z8-226h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2#
DISPLAY_REFRESH2default:default2
142default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/6_DISPLAY_REFRESH.vhd2default:default2
432default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2#
DISPLAY_CONTROL2default:default2
152default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/13_DISPLAY_CONTROL.vhd2default:default2
472default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
TOP2default:default2
162default:default2
12default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/16_TOP.vhd2default:default2
862default:default8@Z8-256h px� 
�
!design %s has unconnected port %s3331*oasys2+
COUNTER__parameterized22default:default2#
COUNTER_LOAD[3]2default:defaultZ8-3331h px� 
�
%s*synth2�
xFinished RTL Elaboration : Time (s): cpu = 00:00:05 ; elapsed = 00:00:05 . Memory (MB): peak = 881.547 ; gain = 308.750
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:05 ; elapsed = 00:00:06 . Memory (MB): peak = 881.547 ; gain = 308.750
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:05 ; elapsed = 00:00:06 . Memory (MB): peak = 881.547 ; gain = 308.750
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0112default:default2
881.5472default:default2
0.0002default:defaultZ17-268h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
�
Parsing XDC File [%s]
179*designutils2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/constrs_1/imports/Real/Nexys4DDR_Master.xdc2default:default8Z20-179h px� 
�
Finished Parsing XDC File [%s]
178*designutils2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/constrs_1/imports/Real/Nexys4DDR_Master.xdc2default:default8Z20-178h px� 
�
�Implementation specific constraints were found while reading constraint file [%s]. These constraints will be ignored for synthesis but will be used in implementation. Impacted constraints are listed in the file [%s].
233*project2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/constrs_1/imports/Real/Nexys4DDR_Master.xdc2default:default2)
.Xil/TOP_propImpl.xdc2default:defaultZ1-236h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0012default:default2
973.8672default:default2
0.0002default:defaultZ17-268h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common24
 Constraint Validation Runtime : 2default:default2
00:00:002default:default2 
00:00:00.0062default:default2
973.8672default:default2
0.0002default:defaultZ17-268h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
~Finished Constraint Validation : Time (s): cpu = 00:00:11 ; elapsed = 00:00:12 . Memory (MB): peak = 973.867 ; gain = 401.070
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Loading part: xc7a100tcsg324-1
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:11 ; elapsed = 00:00:12 . Memory (MB): peak = 973.867 ; gain = 401.070
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:00:11 ; elapsed = 00:00:12 . Memory (MB): peak = 973.867 ; gain = 401.070
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2"
DECODER_OUTPUT2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
!inferring latch for variable '%s'327*oasys2&
DEMUX_OUTPUT_0_reg2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/5_DEMUX.vhd2default:default2
402default:default8@Z8-327h px� 
�
!inferring latch for variable '%s'327*oasys2&
DEMUX_OUTPUT_1_reg2default:default2�
�C:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.srcs/sources_1/imports/Real/5_DEMUX.vhd2default:default2
412default:default8@Z8-327h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:11 ; elapsed = 00:00:12 . Memory (MB): peak = 973.867 ; gain = 401.070
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 24    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 9     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 35    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input      8 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  64 Input      7 Bit        Muxes := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 56    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      4 Bit        Muxes := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 17    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      3 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 77    
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Hierarchical RTL Component report 
2default:defaulth p
x
� 
B
%s
*synth2*
Module CLOCK_DIVIDER 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
A
%s
*synth2)
Module SYNCHRONIZER 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
>
%s
*synth2&
Module DEBOUNCER 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module CLOCK_DIVIDER__parameterized1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
@
%s
*synth2(
Module FLIP_FLOP_X 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
<
%s
*synth2$
Module COUNTER 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
L
%s
*synth24
 Module COUNTER__parameterized2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
8
%s
*synth2 
Module MUX 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module CLOCK_DIVIDER__parameterized3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
<
%s
*synth2$
Module DECODER 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  64 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
D
%s
*synth2,
Module DISPLAY_REFRESH 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input      8 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2k
WPart Resources:
DSPs: 240 (col length:80)
BRAMs: 270 (col length: RAMB18 80 RAMB36 40)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
]
%s
*synth2E
1Warning: Parallel synthesis criteria is not met 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:00:14 ; elapsed = 00:00:16 . Memory (MB): peak = 973.867 ; gain = 401.070
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
2
%s*synth2

ROM:
2default:defaulth px� 
�
%s*synth2s
_+------------+----------------------------------------------+---------------+----------------+
2default:defaulth px� 
�
%s*synth2t
`|Module Name | RTL Object                                   | Depth x Width | Implemented As | 
2default:defaulth px� 
�
%s*synth2s
_+------------+----------------------------------------------+---------------+----------------+
2default:defaulth px� 
�
%s*synth2t
`|TOP         | TOP_DISPLAY_CONTROL/DECODER_0/DECODER_OUTPUT | 64x7          | LUT            | 
2default:defaulth px� 
�
%s*synth2t
`|TOP         | TOP_DISPLAY_CONTROL/DECODER_1/DECODER_OUTPUT | 64x7          | LUT            | 
2default:defaulth px� 
�
%s*synth2t
`|TOP         | TOP_DISPLAY_CONTROL/DECODER_2/DECODER_OUTPUT | 64x7          | LUT            | 
2default:defaulth px� 
�
%s*synth2t
`|TOP         | TOP_DISPLAY_CONTROL/DECODER_3/DECODER_OUTPUT | 64x7          | LUT            | 
2default:defaulth px� 
�
%s*synth2t
`|TOP         | TOP_DISPLAY_CONTROL/DECODER_4/DECODER_OUTPUT | 64x7          | LUT            | 
2default:defaulth px� 
�
%s*synth2t
`|TOP         | TOP_DISPLAY_CONTROL/DECODER_5/DECODER_OUTPUT | 64x7          | LUT            | 
2default:defaulth px� 
�
%s*synth2t
`|TOP         | TOP_DISPLAY_CONTROL/DECODER_6/DECODER_OUTPUT | 64x7          | LUT            | 
2default:defaulth px� 
�
%s*synth2t
`|TOP         | TOP_DISPLAY_CONTROL/DECODER_7/DECODER_OUTPUT | 64x7          | LUT            | 
2default:defaulth px� 
�
%s*synth2t
`+------------+----------------------------------------------+---------------+----------------+

2default:defaulth px� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:00:21 ; elapsed = 00:00:22 . Memory (MB): peak = 973.867 ; gain = 401.070
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
|Finished Timing Optimization : Time (s): cpu = 00:00:21 ; elapsed = 00:00:22 . Memory (MB): peak = 973.867 ; gain = 401.070
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
{Finished Technology Mapping : Time (s): cpu = 00:00:21 ; elapsed = 00:00:22 . Memory (MB): peak = 982.797 ; gain = 410.000
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
uFinished IO Insertion : Time (s): cpu = 00:00:25 ; elapsed = 00:00:26 . Memory (MB): peak = 988.637 ; gain = 415.840
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:00:25 ; elapsed = 00:00:26 . Memory (MB): peak = 988.637 ; gain = 415.840
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:00:25 ; elapsed = 00:00:26 . Memory (MB): peak = 988.637 ; gain = 415.840
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:00:25 ; elapsed = 00:00:26 . Memory (MB): peak = 988.637 ; gain = 415.840
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:25 ; elapsed = 00:00:26 . Memory (MB): peak = 988.637 ; gain = 415.840
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:00:25 ; elapsed = 00:00:26 . Memory (MB): peak = 988.637 ; gain = 415.840
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23

Static Shift Register Report:
2default:defaulth p
x
� 
�
%s
*synth2�
�+------------+-------------------------------------------------------------------+--------+-------+--------------+--------------------+-------------------+--------+---------+
2default:defaulth p
x
� 
�
%s
*synth2�
�|Module Name | RTL Name                                                          | Length | Width | Reset Signal | Pull out first Reg | Pull out last Reg | SRL16E | SRLC32E | 
2default:defaulth p
x
� 
�
%s
*synth2�
�+------------+-------------------------------------------------------------------+--------+-------+--------------+--------------------+-------------------+--------+---------+
2default:defaulth p
x
� 
�
%s
*synth2�
�|TOP         | TOP_CONDITIONER/DEBOUNCER_MASTER_RESET/DEBOUNCER_INPUT_PRV_reg[0] | 4      | 1     | YES          | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|TOP         | TOP_CONDITIONER/DEBOUNCER_MODE/DEBOUNCER_INPUT_PRV_reg[0]         | 4      | 1     | YES          | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|TOP         | TOP_CONDITIONER/DEBOUNCER_RESET/DEBOUNCER_INPUT_PRV_reg[0]        | 4      | 1     | YES          | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|TOP         | TOP_CONDITIONER/DEBOUNCER_START_PAUSE/DEBOUNCER_INPUT_PRV_reg[0]  | 4      | 1     | YES          | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�+------------+-------------------------------------------------------------------+--------+-------+--------------+--------------------+-------------------+--------+---------+

2default:defaulth p
x
� 
�
%s
*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
| |BlackBox name |Instances |
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
D
%s*synth2,
|      |Cell   |Count |
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
D
%s*synth2,
|1     |BUFG   |     4|
2default:defaulth px� 
D
%s*synth2,
|2     |CARRY4 |    24|
2default:defaulth px� 
D
%s*synth2,
|3     |LUT1   |     9|
2default:defaulth px� 
D
%s*synth2,
|4     |LUT2   |     7|
2default:defaulth px� 
D
%s*synth2,
|5     |LUT3   |    51|
2default:defaulth px� 
D
%s*synth2,
|6     |LUT4   |    78|
2default:defaulth px� 
D
%s*synth2,
|7     |LUT5   |   128|
2default:defaulth px� 
D
%s*synth2,
|8     |LUT6   |    93|
2default:defaulth px� 
D
%s*synth2,
|9     |MUXF7  |    28|
2default:defaulth px� 
D
%s*synth2,
|10    |SRL16E |     4|
2default:defaulth px� 
D
%s*synth2,
|11    |FDCE   |   228|
2default:defaulth px� 
D
%s*synth2,
|12    |FDPE   |     2|
2default:defaulth px� 
D
%s*synth2,
|13    |FDRE   |    11|
2default:defaulth px� 
D
%s*synth2,
|14    |LD     |     4|
2default:defaulth px� 
D
%s*synth2,
|15    |IBUF   |     5|
2default:defaulth px� 
D
%s*synth2,
|16    |OBUF   |    15|
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
E
%s
*synth2-

Report Instance Areas: 
2default:defaulth p
x
� 
z
%s
*synth2b
N+------+------------------------------+------------------------------+------+
2default:defaulth p
x
� 
z
%s
*synth2b
N|      |Instance                      |Module                        |Cells |
2default:defaulth p
x
� 
z
%s
*synth2b
N+------+------------------------------+------------------------------+------+
2default:defaulth p
x
� 
z
%s
*synth2b
N|1     |top                           |                              |   691|
2default:defaulth p
x
� 
z
%s
*synth2b
N|2     |  TOP_CHRONO                  |CHRONO                        |   358|
2default:defaulth p
x
� 
z
%s
*synth2b
N|3     |    CHRONO_0                  |CHRONO_COUNTER                |    80|
2default:defaulth p
x
� 
z
%s
*synth2b
N|4     |      COUNTER_0               |COUNTER_14                    |    10|
2default:defaulth p
x
� 
z
%s
*synth2b
N|5     |      COUNTER_1               |COUNTER_15                    |    10|
2default:defaulth p
x
� 
z
%s
*synth2b
N|6     |      COUNTER_2               |COUNTER_16                    |    10|
2default:defaulth p
x
� 
z
%s
*synth2b
N|7     |      COUNTER_3               |COUNTER_17                    |    10|
2default:defaulth p
x
� 
z
%s
*synth2b
N|8     |      COUNTER_4               |COUNTER_18                    |    10|
2default:defaulth p
x
� 
z
%s
*synth2b
N|9     |      COUNTER_5               |COUNTER__parameterized2_19    |     8|
2default:defaulth p
x
� 
z
%s
*synth2b
N|10    |      COUNTER_6               |COUNTER_20                    |    10|
2default:defaulth p
x
� 
z
%s
*synth2b
N|11    |      COUNTER_7               |COUNTER__parameterized2_21    |     9|
2default:defaulth p
x
� 
z
%s
*synth2b
N|12    |      FFX                     |FLIP_FLOP_X_22                |     3|
2default:defaulth p
x
� 
z
%s
*synth2b
N|13    |    CHRONO_1                  |CHRONO_COUNTER_6              |   191|
2default:defaulth p
x
� 
z
%s
*synth2b
N|14    |      COUNTER_0               |COUNTER                       |    27|
2default:defaulth p
x
� 
z
%s
*synth2b
N|15    |      COUNTER_1               |COUNTER_8                     |    20|
2default:defaulth p
x
� 
z
%s
*synth2b
N|16    |      COUNTER_2               |COUNTER_9                     |    27|
2default:defaulth p
x
� 
z
%s
*synth2b
N|17    |      COUNTER_3               |COUNTER_10                    |    20|
2default:defaulth p
x
� 
z
%s
*synth2b
N|18    |      COUNTER_4               |COUNTER_11                    |    27|
2default:defaulth p
x
� 
z
%s
*synth2b
N|19    |      COUNTER_5               |COUNTER__parameterized2       |    16|
2default:defaulth p
x
� 
z
%s
*synth2b
N|20    |      COUNTER_6               |COUNTER_12                    |    34|
2default:defaulth p
x
� 
z
%s
*synth2b
N|21    |      COUNTER_7               |COUNTER__parameterized2_13    |    17|
2default:defaulth p
x
� 
z
%s
*synth2b
N|22    |      FFX                     |FLIP_FLOP_X                   |     3|
2default:defaulth p
x
� 
z
%s
*synth2b
N|23    |    CHRONO_CLOCK_DIVIDER      |CLOCK_DIVIDER__parameterized1 |    82|
2default:defaulth p
x
� 
z
%s
*synth2b
N|24    |    CHRONO_DEMUX              |COUNTER_DEMUX                 |     5|
2default:defaulth p
x
� 
z
%s
*synth2b
N|25    |      DEMUX_0                 |DEMUX                         |     3|
2default:defaulth p
x
� 
z
%s
*synth2b
N|26    |      DEMUX_1                 |DEMUX_7                       |     2|
2default:defaulth p
x
� 
z
%s
*synth2b
N|27    |  TOP_CLOCK_DIVIDER           |CLOCK_DIVIDER                 |    82|
2default:defaulth p
x
� 
z
%s
*synth2b
N|28    |  TOP_CONDITIONER             |CONDITIONER                   |   131|
2default:defaulth p
x
� 
z
%s
*synth2b
N|29    |    DEBOUNCER_MASTER_RESET    |DEBOUNCER                     |    29|
2default:defaulth p
x
� 
z
%s
*synth2b
N|30    |    DEBOUNCER_MODE            |DEBOUNCER_0                   |    29|
2default:defaulth p
x
� 
z
%s
*synth2b
N|31    |    DEBOUNCER_RESET           |DEBOUNCER_1                   |    29|
2default:defaulth p
x
� 
z
%s
*synth2b
N|32    |    DEBOUNCER_START_PAUSE     |DEBOUNCER_2                   |    29|
2default:defaulth p
x
� 
z
%s
*synth2b
N|33    |    SYNCHRONIZER_MASTER_RESET |SYNCHRONIZER                  |     6|
2default:defaulth p
x
� 
z
%s
*synth2b
N|34    |    SYNCHRONIZER_MODE         |SYNCHRONIZER_3                |     3|
2default:defaulth p
x
� 
z
%s
*synth2b
N|35    |    SYNCHRONIZER_RESET        |SYNCHRONIZER_4                |     3|
2default:defaulth p
x
� 
z
%s
*synth2b
N|36    |    SYNCHRONIZER_START_PAUSE  |SYNCHRONIZER_5                |     3|
2default:defaulth p
x
� 
z
%s
*synth2b
N|37    |  TOP_DISPLAY_CONTROL         |DISPLAY_CONTROL               |    96|
2default:defaulth p
x
� 
z
%s
*synth2b
N|38    |    CONTROL_DISPLAY_REFRESH   |DISPLAY_REFRESH               |    14|
2default:defaulth p
x
� 
z
%s
*synth2b
N|39    |    REFRESH_CLOCK_DIVIDER     |CLOCK_DIVIDER__parameterized3 |    82|
2default:defaulth p
x
� 
z
%s
*synth2b
N+------+------------------------------+------------------------------+------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:00:25 ; elapsed = 00:00:26 . Memory (MB): peak = 988.637 ; gain = 415.840
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
r
%s
*synth2Z
FSynthesis finished with 0 errors, 0 critical warnings and 2 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
~Synthesis Optimization Runtime : Time (s): cpu = 00:00:18 ; elapsed = 00:00:24 . Memory (MB): peak = 988.637 ; gain = 323.520
2default:defaulth p
x
� 
�
%s
*synth2�
Synthesis Optimization Complete : Time (s): cpu = 00:00:25 ; elapsed = 00:00:27 . Memory (MB): peak = 988.637 ; gain = 415.840
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0062default:default2
988.6372default:default2
0.0002default:defaultZ17-268h px� 
f
-Analyzing %s Unisim elements for replacement
17*netlist2
562default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
12default:default2
22default:defaultZ31-138h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
997.5512default:default2
0.0002default:defaultZ17-268h px� 
�
!Unisim Transformation Summary:
%s111*project2�
r  A total of 4 instances were transformed.
  LD => LDCE: 2 instances
  LD => LDCE (inverted pins: G): 2 instances
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
982default:default2
62default:default2
02default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2"
synth_design: 2default:default2
00:00:302default:default2
00:00:322default:default2
997.5512default:default2
698.7462default:defaultZ17-268h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
997.5512default:default2
0.0002default:defaultZ17-268h px� 
K
"No constraints selected for write.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2�
oC:/Users/Ignacio/Downloads/SED/VHDL/Proyectos Vivado/cronometro_competi/cronometro_competi.runs/synth_1/TOP.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2p
\Executing : report_utilization -file TOP_utilization_synth.rpt -pb TOP_utilization_synth.pb
2default:defaulth px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Thu Jan 16 19:18:59 20202default:defaultZ17-206h px� 


End Record