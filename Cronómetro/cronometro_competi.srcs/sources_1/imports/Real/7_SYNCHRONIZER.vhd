----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 17.12.2019
-- Design Name:
-- Module Name: SYNCHRONIZER - BEHAVIORAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 14.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity SYNCHRONIZER is
    generic(
		SYNCHRONIZER_N : positive := 1;
		SYNCHRONIZER_INDEX : positive := 2
	);
	port (
		SYNCHRONIZER_CLOCK : in std_logic;
		SYNCHRONIZER_INPUT : in std_logic_vector(SYNCHRONIZER_N-1 downto 0);
		SYNCHRONIZER_RESET : in std_logic;
		SYNCHRONIZER_OUTPUT : out std_logic_vector(SYNCHRONIZER_N-1 downto 0)
	);
end SYNCHRONIZER;

architecture Behavioral of SYNCHRONIZER is
	signal REGISTER_SIGNAL: std_logic_vector(SYNCHRONIZER_INDEX downto 0) := (others =>'0');
begin
	process(SYNCHRONIZER_CLOCK, SYNCHRONIZER_RESET)
	begin
		if(SYNCHRONIZER_RESET = '1') then
			REGISTER_SIGNAL <= (others => '0');
		elsif(SYNCHRONIZER_CLOCK'event and SYNCHRONIZER_CLOCK = '1') then
			REGISTER_SIGNAL <= REGISTER_SIGNAL(SYNCHRONIZER_INDEX-1 downto 0) & SYNCHRONIZER_INPUT;
		end if;
	end process;
	SYNCHRONIZER_OUTPUT(0) <= REGISTER_SIGNAL(SYNCHRONIZER_INDEX);
end architecture BEHAVIORAL;