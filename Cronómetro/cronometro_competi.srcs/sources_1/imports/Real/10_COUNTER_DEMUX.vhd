----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 09.01.2020
-- Design Name:
-- Module Name: COUNTER_DEMUX - STRUCTURAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 14.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity COUNTER_DEMUX is
	generic(
		COUNTER_DEMUX_N : positive := 1
	);
	port(
		COUNTER_DEMUX_OUTPUT_SELECTION : in std_logic;
		COUNTER_DEMUX_INPUT_0 : in std_logic_vector(COUNTER_DEMUX_N-1 downto 0);
		COUNTER_DEMUX_INPUT_1 : in std_logic_vector(COUNTER_DEMUX_N-1 downto 0);
		COUNTER_DEMUX_OUTPUT_0_0 : out std_logic_vector(COUNTER_DEMUX_N-1 downto 0);
		COUNTER_DEMUX_OUTPUT_0_1 : out std_logic_vector(COUNTER_DEMUX_N-1 downto 0);
		COUNTER_DEMUX_OUTPUT_1_0 : out std_logic_vector(COUNTER_DEMUX_N-1 downto 0);
		COUNTER_DEMUX_OUTPUT_1_1 : out std_logic_vector(COUNTER_DEMUX_N-1 downto 0)
	);
end entity COUNTER_DEMUX;

architecture STRUCTURAL of COUNTER_DEMUX is
	component  DEMUX
		generic(
			DEMUX_N : positive := 1
		);
		port(
			DEMUX_OUTPUT_SELECTION : in std_logic;
			DEMUX_INPUT : in std_logic_vector(DEMUX_N-1 downto 0);
			DEMUX_OUTPUT_0 : out std_logic_vector(DEMUX_N-1 downto 0);
			DEMUX_OUTPUT_1 : out std_logic_vector(DEMUX_N-1 downto 0)
		);
	end component;
begin
	DEMUX_0 : DEMUX
		generic map(
			DEMUX_N					=> COUNTER_DEMUX_N
		)
		port map(
			DEMUX_OUTPUT_SELECTION	=> COUNTER_DEMUX_OUTPUT_SELECTION,
			DEMUX_INPUT				=> COUNTER_DEMUX_INPUT_0,
			DEMUX_OUTPUT_0			=> COUNTER_DEMUX_OUTPUT_0_0,
			DEMUX_OUTPUT_1			=> COUNTER_DEMUX_OUTPUT_0_1
		);
	DEMUX_1 : DEMUX
		generic map(
			DEMUX_N					=> COUNTER_DEMUX_N
		)
		port map(
			DEMUX_OUTPUT_SELECTION	=> COUNTER_DEMUX_OUTPUT_SELECTION,
			DEMUX_INPUT				=> COUNTER_DEMUX_INPUT_1,
			DEMUX_OUTPUT_0			=> COUNTER_DEMUX_OUTPUT_1_0,
			DEMUX_OUTPUT_1			=> COUNTER_DEMUX_OUTPUT_1_1
		);
end architecture STRUCTURAL;