----------------------------------------------------------------------------------
-- Company: 
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 12.01.2020 17:12:29
-- Design Name: 
-- Module Name: TOP - BEHAVIORAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 16.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TOP is
	generic(
		TOP_O : positive := 1;--NO TOCAR
		TOP_N : positive := 4;
		TOP_M : positive := 7;--NO TOCAR
		TOP_P : positive := 8;--NO TOCAR
		TOP_CONDITIONER_SYNCHRONIZER_INDEX : positive := 2;
		TOP_CONDITIONER_DEBOUNCER_DELAY : positive := 1000;
		TOP_INPUT_FREQUENCY		: positive := 100000000;	--RELOJ PLACA 100 MHz
		TOP_GLOBAL_FREQUENCY	: positive := 100000;		--MAYOR QUE TOP_CHRONO_FREQUENCY
		TOP_CHRONO_FREQUENCY	: positive := 10000;		--MAYOR FRECUENCIA CHRONO
		TOP_DISPLAY_FREQUENCY	: positive := 400;			--FRECUENCIA DISPLAY=50*TOP_P
		TOP_CHRONO_0_LIMIT_0	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_0_LIMIT_1	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_0_LIMIT_2	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_0_LIMIT_3	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_0_LIMIT_4	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_0_LIMIT_5	: positive := 5;--POR DEFECTO PONER 5
		TOP_CHRONO_0_LIMIT_6	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_0_LIMIT_7	: positive := 5;--POR DEFECTO PONER 5
		TOP_CHRONO_0_MODE	: std_logic := '0';--POR DEFECTO PONER '0' (FORWARD)
		TOP_CHRONO_0_LOAD_0	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_0_LOAD_1	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_0_LOAD_2	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_0_LOAD_3	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_0_LOAD_4	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_0_LOAD_5	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_0_LOAD_6	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_0_LOAD_7	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_1_LIMIT_0	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_1_LIMIT_1	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_1_LIMIT_2	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_1_LIMIT_3	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_1_LIMIT_4	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_1_LIMIT_5	: positive := 5;--POR DEFECTO PONER 5
		TOP_CHRONO_1_LIMIT_6	: positive := 9;--POR DEFECTO PONER 9
		TOP_CHRONO_1_LIMIT_7	: positive := 5;--POR DEFECTO PONER 5
		TOP_CHRONO_1_MODE	: std_logic := '1';--POR DEFECTO PONER '1' (BACKWARD)
		TOP_CHRONO_1_LOAD_0	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_1_LOAD_1	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_1_LOAD_2	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_1_LOAD_3	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_1_LOAD_4	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_1_LOAD_5	: integer := 3;--<2^CHRONO_N
		TOP_CHRONO_1_LOAD_6	: integer := 0;--<2^CHRONO_N
		TOP_CHRONO_1_LOAD_7	: integer := 0--<2^CHRONO_N
	);
	port(
		TOP_CLOCK : in std_logic;
		TOP_MODE : in std_logic;
		TOP_RESET : in std_logic;
		TOP_START_PAUSE : in std_logic;
		TOP_MASTER_RESET : in std_logic;
		TOP_DIGIT_DATA : out std_logic_vector(TOP_M-1 downto 0);
		TOP_DIGIT_POSITION : out std_logic_vector(TOP_P-1 downto 0)
		--TOP_MODE_LED : out std_logic;
		--TOP_RESET_LED : out std_logic;
		--TOP_START_PAUSE_LED : out std_logic;
		--TOP_MASTER_RESET_LED : out std_logic
	);
end entity TOP;

architecture BEHAVIORAL of TOP is
	component CLOCK_DIVIDER
		generic(
			INPUT_FREQUENCY : positive := 100000000;
			OUTPUT_FREQUENCY : positive := 10000
		);
		port(
			CLOCK_DIVIDER_CLOCK_IN : in std_logic;
			CLOCK_DIVIDER_RESET : in std_logic;
			CLOCK_DIVIDER_CLOCK_OUT : out std_logic
		);
	end component;
	component CONDITIONER
		generic(
			CONDITIONER_N : positive := 1;
			CONDITIONER_SYNCHRONIZER_INDEX : positive := 2;
			CONDITIONER_DEBOUNCER_DELAY : positive := 1000
		);
		port(
			CONDITIONER_INPUT_CLOCK : in std_logic;
			CONDITIONER_INPUT_MODE : in std_logic;
			CONDITIONER_INPUT_RESET : in std_logic;
			CONDITIONER_INPUT_START_PAUSE : in std_logic;
			CONDITIONER_INPUT_MASTER_RESET : in std_logic;
			CONDITIONER_OUTPUT_MODE : out std_logic;
			CONDITIONER_OUTPUT_RESET : out std_logic;
			CONDITIONER_OUTPUT_START_PAUSE : out std_logic;
			CONDITIONER_OUTPUT_MASTER_RESET : out std_logic
		);
	end component;
	component CHRONO
		generic(
			CHRONO_O : positive := 1;
			CHRONO_N : positive := 4;
			CHRONO_INPUT_FREQUENCY		: positive := 100000000;
			CHRONO_WORKING_FREQUENCY	: positive := 10000;
			CHRONO_0_LIMIT_0	: positive := 9;
			CHRONO_0_LIMIT_1	: positive := 9;
			CHRONO_0_LIMIT_2	: positive := 9;
			CHRONO_0_LIMIT_3	: positive := 9;
			CHRONO_0_LIMIT_4	: positive := 9;
			CHRONO_0_LIMIT_5	: positive := 5;
			CHRONO_0_LIMIT_6	: positive := 9;
			CHRONO_0_LIMIT_7	: positive := 5;
			CHRONO_0_MODE	: std_logic := '0';
			CHRONO_0_LOAD_0	: integer := 0;--<2^CHRONO_N
			CHRONO_0_LOAD_1	: integer := 0;--<2^CHRONO_N
			CHRONO_0_LOAD_2	: integer := 0;--<2^CHRONO_N
			CHRONO_0_LOAD_3	: integer := 0;--<2^CHRONO_N
			CHRONO_0_LOAD_4	: integer := 0;--<2^CHRONO_N
			CHRONO_0_LOAD_5	: integer := 0;--<2^CHRONO_N
			CHRONO_0_LOAD_6	: integer := 0;--<2^CHRONO_N
			CHRONO_0_LOAD_7	: integer := 0;--<2^CHRONO_N
			CHRONO_1_LIMIT_0	: positive := 9;
			CHRONO_1_LIMIT_1	: positive := 9;
			CHRONO_1_LIMIT_2	: positive := 9;
			CHRONO_1_LIMIT_3	: positive := 9;
			CHRONO_1_LIMIT_4	: positive := 9;
			CHRONO_1_LIMIT_5	: positive := 5;
			CHRONO_1_LIMIT_6	: positive := 9;
			CHRONO_1_LIMIT_7	: positive := 5;
			CHRONO_1_MODE	: std_logic := '1';
			CHRONO_1_LOAD_0	: integer := 0;--<2^CHRONO_N
			CHRONO_1_LOAD_1	: integer := 0;--<2^CHRONO_N
			CHRONO_1_LOAD_2	: integer := 0;--<2^CHRONO_N
			CHRONO_1_LOAD_3	: integer := 0;--<2^CHRONO_N
			CHRONO_1_LOAD_4	: integer := 0;--<2^CHRONO_N
			CHRONO_1_LOAD_5	: integer := 3;--<2^CHRONO_N
			CHRONO_1_LOAD_6	: integer := 0;--<2^CHRONO_N
			CHRONO_1_LOAD_7	: integer := 0--<2^CHRONO_N
		);
		port(
			CHRONO_CLOCK : in std_logic;
			CHRONO_MODE : in std_logic;
			CHRONO_RESET : in std_logic;
			CHRONO_START_PAUSE : in std_logic;
			CHRONO_MASTER_RESET : in std_logic;
			CHRONO_OUTPUT_0 : out std_logic_vector(CHRONO_N-1 downto 0);
			CHRONO_OUTPUT_1 : out std_logic_vector(CHRONO_N-1 downto 0);
			CHRONO_OUTPUT_2 : out std_logic_vector(CHRONO_N-1 downto 0);
			CHRONO_OUTPUT_3 : out std_logic_vector(CHRONO_N-1 downto 0);
			CHRONO_OUTPUT_4 : out std_logic_vector(CHRONO_N-1 downto 0);
			CHRONO_OUTPUT_5 : out std_logic_vector(CHRONO_N-1 downto 0);
			CHRONO_OUTPUT_6 : out std_logic_vector(CHRONO_N-1 downto 0);
			CHRONO_OUTPUT_7 : out std_logic_vector(CHRONO_N-1 downto 0)
		);
	end component;
	component DISPLAY_CONTROL
		generic(
			DISPLAY_CONTROL_N			: positive := 4;
			DISPLAY_CONTROL_M			: positive := 7;
			DISPLAY_CONTROL_P			: positive := 8;
			DISPLAY_INPUT_FREQUENCY		: positive := 10000;
			DISPLAY_WORKING_FREQUENCY	: positive := 400
		);
		port(
			DISPLAY_CONTROL_CLOCK : in std_logic;
			DISPLAY_CONTROL_RESET : in std_logic;
			DISPLAY_CONTROL_INPUT_0 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
			DISPLAY_CONTROL_INPUT_1 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
			DISPLAY_CONTROL_INPUT_2 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
			DISPLAY_CONTROL_INPUT_3 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
			DISPLAY_CONTROL_INPUT_4 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
			DISPLAY_CONTROL_INPUT_5 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
			DISPLAY_CONTROL_INPUT_6 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
			DISPLAY_CONTROL_INPUT_7 : in std_logic_vector(DISPLAY_CONTROL_N-1 downto 0);
			DISPLAY_CONTROL_OUTPUT_DIGIT_DATA : out std_logic_vector(DISPLAY_CONTROL_M-1 downto 0);
			DISPLAY_CONTROL_OUTPUT_DIGIT_POSITION : out std_logic_vector(DISPLAY_CONTROL_P-1 downto 0)
		);
	end component;  
	signal CLOCK: std_logic;
	signal MODE: std_logic;
	signal RESET: std_logic;
	signal START_PAUSE: std_logic;
	signal MASTER_RESET: std_logic;
	signal NUMBER_0: std_logic_vector(TOP_N-1 downto 0);
	signal NUMBER_1: std_logic_vector(TOP_N-1 downto 0);
	signal NUMBER_2: std_logic_vector(TOP_N-1 downto 0);
	signal NUMBER_3: std_logic_vector(TOP_N-1 downto 0);
	signal NUMBER_4: std_logic_vector(TOP_N-1 downto 0);
	signal NUMBER_5: std_logic_vector(TOP_N-1 downto 0);
	signal NUMBER_6: std_logic_vector(TOP_N-1 downto 0);
	signal NUMBER_7: std_logic_vector(TOP_N-1 downto 0);  
begin
	TOP_CLOCK_DIVIDER: CLOCK_DIVIDER
		generic map(
			INPUT_FREQUENCY					=> TOP_INPUT_FREQUENCY,
			OUTPUT_FREQUENCY				=> TOP_GLOBAL_FREQUENCY
		)
		port map(
			CLOCK_DIVIDER_CLOCK_IN			=> TOP_CLOCK,
			CLOCK_DIVIDER_RESET				=> MASTER_RESET,
			CLOCK_DIVIDER_CLOCK_OUT			=> CLOCK
		);
	TOP_CONDITIONER: CONDITIONER
		generic map(
			CONDITIONER_N					=> TOP_O,
			CONDITIONER_SYNCHRONIZER_INDEX	=> TOP_CONDITIONER_SYNCHRONIZER_INDEX,
			CONDITIONER_DEBOUNCER_DELAY		=> TOP_CONDITIONER_DEBOUNCER_DELAY
		)
		port map(
			CONDITIONER_INPUT_CLOCK			=> CLOCK,
			CONDITIONER_INPUT_MODE			=> TOP_MODE,
			CONDITIONER_INPUT_RESET			=> TOP_RESET,
			CONDITIONER_INPUT_START_PAUSE	=> TOP_START_PAUSE,
			CONDITIONER_INPUT_MASTER_RESET	=> TOP_MASTER_RESET,
			CONDITIONER_OUTPUT_MODE			=> MODE,
			CONDITIONER_OUTPUT_RESET		=> RESET,
			CONDITIONER_OUTPUT_START_PAUSE	=> START_PAUSE,
			CONDITIONER_OUTPUT_MASTER_RESET	=> MASTER_RESET
		);
	TOP_CHRONO: CHRONO
		generic map(
			CHRONO_O					=> TOP_O,
			CHRONO_N					=> TOP_N,
			CHRONO_INPUT_FREQUENCY		=> TOP_GLOBAL_FREQUENCY,
			CHRONO_WORKING_FREQUENCY	=> TOP_CHRONO_FREQUENCY,
			CHRONO_0_LIMIT_0			=> TOP_CHRONO_0_LIMIT_0,
			CHRONO_0_LIMIT_1			=> TOP_CHRONO_0_LIMIT_1,
			CHRONO_0_LIMIT_2			=> TOP_CHRONO_0_LIMIT_2,
			CHRONO_0_LIMIT_3			=> TOP_CHRONO_0_LIMIT_3,
			CHRONO_0_LIMIT_4			=> TOP_CHRONO_0_LIMIT_4,
			CHRONO_0_LIMIT_5			=> TOP_CHRONO_0_LIMIT_5,
			CHRONO_0_LIMIT_6			=> TOP_CHRONO_0_LIMIT_6,
			CHRONO_0_LIMIT_7			=> TOP_CHRONO_0_LIMIT_7,
			CHRONO_0_MODE				=> TOP_CHRONO_0_MODE,
			CHRONO_0_LOAD_0				=> TOP_CHRONO_0_LOAD_0,
			CHRONO_0_LOAD_1				=> TOP_CHRONO_0_LOAD_1,
			CHRONO_0_LOAD_2				=> TOP_CHRONO_0_LOAD_2,
			CHRONO_0_LOAD_3				=> TOP_CHRONO_0_LOAD_3,
			CHRONO_0_LOAD_4				=> TOP_CHRONO_0_LOAD_4,
			CHRONO_0_LOAD_5				=> TOP_CHRONO_0_LOAD_5,
			CHRONO_0_LOAD_6				=> TOP_CHRONO_0_LOAD_6,
			CHRONO_0_LOAD_7				=> TOP_CHRONO_0_LOAD_7,
			CHRONO_1_LIMIT_0			=> TOP_CHRONO_1_LIMIT_0,
			CHRONO_1_LIMIT_1			=> TOP_CHRONO_1_LIMIT_1,
			CHRONO_1_LIMIT_2			=> TOP_CHRONO_1_LIMIT_2,
			CHRONO_1_LIMIT_3			=> TOP_CHRONO_1_LIMIT_3,
			CHRONO_1_LIMIT_4			=> TOP_CHRONO_1_LIMIT_4,
			CHRONO_1_LIMIT_5			=> TOP_CHRONO_1_LIMIT_5,
			CHRONO_1_LIMIT_6			=> TOP_CHRONO_1_LIMIT_6,
			CHRONO_1_LIMIT_7			=> TOP_CHRONO_1_LIMIT_7,
			CHRONO_1_MODE				=> TOP_CHRONO_1_MODE,
			CHRONO_1_LOAD_0				=> TOP_CHRONO_1_LOAD_0,
			CHRONO_1_LOAD_1				=> TOP_CHRONO_1_LOAD_1,
			CHRONO_1_LOAD_2				=> TOP_CHRONO_1_LOAD_2,
			CHRONO_1_LOAD_3				=> TOP_CHRONO_1_LOAD_3,
			CHRONO_1_LOAD_4				=> TOP_CHRONO_1_LOAD_4,
			CHRONO_1_LOAD_5				=> TOP_CHRONO_1_LOAD_5,
			CHRONO_1_LOAD_6				=> TOP_CHRONO_1_LOAD_6,
			CHRONO_1_LOAD_7				=> TOP_CHRONO_1_LOAD_7
		)
		port map(
			CHRONO_CLOCK		=> CLOCK,
			CHRONO_MODE			=> MODE,
			CHRONO_RESET		=> RESET,
			CHRONO_START_PAUSE	=> START_PAUSE,
			CHRONO_MASTER_RESET	=> MASTER_RESET,
			CHRONO_OUTPUT_0		=> NUMBER_0,
			CHRONO_OUTPUT_1		=> NUMBER_1,
			CHRONO_OUTPUT_2		=> NUMBER_2,
			CHRONO_OUTPUT_3		=> NUMBER_3,
			CHRONO_OUTPUT_4		=> NUMBER_4,
			CHRONO_OUTPUT_5		=> NUMBER_5,
			CHRONO_OUTPUT_6		=> NUMBER_6,
			CHRONO_OUTPUT_7		=> NUMBER_7
		);
	TOP_DISPLAY_CONTROL: DISPLAY_CONTROL
		generic map(
			DISPLAY_CONTROL_N					=> TOP_N,
			DISPLAY_CONTROL_M					=> TOP_M,
			DISPLAY_CONTROL_P					=> TOP_P,
			DISPLAY_INPUT_FREQUENCY				=> TOP_GLOBAL_FREQUENCY,
			DISPLAY_WORKING_FREQUENCY			=> TOP_DISPLAY_FREQUENCY
		)
		port map(                                 
			DISPLAY_CONTROL_CLOCK					=> CLOCK,
			DISPLAY_CONTROL_RESET					=> MASTER_RESET,
			DISPLAY_CONTROL_INPUT_0					=> NUMBER_0,
			DISPLAY_CONTROL_INPUT_1					=> NUMBER_1,
			DISPLAY_CONTROL_INPUT_2					=> NUMBER_2,
			DISPLAY_CONTROL_INPUT_3					=> NUMBER_3,
			DISPLAY_CONTROL_INPUT_4					=> NUMBER_4,
			DISPLAY_CONTROL_INPUT_5					=> NUMBER_5,
			DISPLAY_CONTROL_INPUT_6					=> NUMBER_6,
			DISPLAY_CONTROL_INPUT_7					=> NUMBER_7, 
			DISPLAY_CONTROL_OUTPUT_DIGIT_DATA		=> TOP_DIGIT_DATA,
			DISPLAY_CONTROL_OUTPUT_DIGIT_POSITION	=> TOP_DIGIT_POSITION
		);
		--TOP_MODE_LED			<= MODE;
		--TOP_RESET_LED			<= RESET;
		--TOP_START_PAUSE_LED	<= START_PAUSE;
		--TOP_MASTER_RESET_LED	<= MASTER_RESET;
end architecture BEHAVIORAL;
