----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 09.01.2020
-- Design Name:
-- Module Name: DECODER - BEHAVIORAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 16.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DECODER is
	generic(
		DECODER_N : positive := 4;
		DECODER_M : positive := 7
	);
	port(
		DECODER_INPUT : in std_logic_vector(DECODER_N-1 downto 0);
		DECODER_OUTPUT : out std_logic_vector(DECODER_M-1 downto 0)
	);
end entity DECODER;

architecture BEHAVIORAL of DECODER is
begin
	process(DECODER_INPUT)
		variable V_COUNTER : integer;
	begin
		V_COUNTER := to_integer(unsigned(DECODER_INPUT));
		case V_COUNTER is
			when 0	=> DECODER_OUTPUT <= "1000000";--0
			when 1	=> DECODER_OUTPUT <= "1111001";--1
			when 2	=> DECODER_OUTPUT <= "0100100";--2
			when 3	=> DECODER_OUTPUT <= "0110000";--3
			when 4	=> DECODER_OUTPUT <= "0011001";--4
			when 5	=> DECODER_OUTPUT <= "0010010";--5
			when 6	=> DECODER_OUTPUT <= "0000010";--6
			when 7	=> DECODER_OUTPUT <= "1111000";--7
			when 8	=> DECODER_OUTPUT <= "0000000";--8
			when 9	=> DECODER_OUTPUT <= "0011000";--9
			when 10	=> DECODER_OUTPUT <= "0001000";--A
			when 11	=> DECODER_OUTPUT <= "0000011";--b
			when 12	=> DECODER_OUTPUT <= "0100111";--c
			when 13	=> DECODER_OUTPUT <= "0100001";--d
			when 14	=> DECODER_OUTPUT <= "0000110";--E
			when 15	=> DECODER_OUTPUT <= "0001110";--F
			when 16	=> DECODER_OUTPUT <= "1111110";--
			when 17	=> DECODER_OUTPUT <= "1111101";--
			when 18	=> DECODER_OUTPUT <= "0111111";--
			when 19	=> DECODER_OUTPUT <= "1101111";--
			when 20	=> DECODER_OUTPUT <= "1110111";--
			when 21	=> DECODER_OUTPUT <= "1111011";--
			when 22	=> DECODER_OUTPUT <= "0111111";--
			when 23	=> DECODER_OUTPUT <= "1011111";--
			when 24	=> DECODER_OUTPUT <= "1111110";--
			when 25	=> DECODER_OUTPUT <= "1111101";--
			when 26	=> DECODER_OUTPUT <= "0111111";--
			when 27	=> DECODER_OUTPUT <= "1101111";--
			when 28	=> DECODER_OUTPUT <= "1110111";--
			when 29	=> DECODER_OUTPUT <= "1111011";--
			when 30	=> DECODER_OUTPUT <= "0111111";--
			when 31	=> DECODER_OUTPUT <= "1011111";--
			when 32	=> DECODER_OUTPUT <= "1111110";--
			when 33	=> DECODER_OUTPUT <= "1111101";--
			when 34	=> DECODER_OUTPUT <= "0111111";--
			when 35	=> DECODER_OUTPUT <= "1101111";--
			when 36	=> DECODER_OUTPUT <= "1110111";--
			when 37	=> DECODER_OUTPUT <= "1111011";--
			when 38	=> DECODER_OUTPUT <= "0111111";--
			when 39	=> DECODER_OUTPUT <= "1011111";--
			when 40	=> DECODER_OUTPUT <= "1111110";--
			when 41	=> DECODER_OUTPUT <= "1111101";--
			when 42	=> DECODER_OUTPUT <= "0111111";--
			when 43	=> DECODER_OUTPUT <= "1101111";--
			when 44	=> DECODER_OUTPUT <= "1110111";--
			when 45	=> DECODER_OUTPUT <= "1111011";--
			when 46	=> DECODER_OUTPUT <= "0111111";--
			when 47	=> DECODER_OUTPUT <= "1011111";--
			when 48	=> DECODER_OUTPUT <= "1111110";--
			when 49	=> DECODER_OUTPUT <= "1111101";--
			when 50	=> DECODER_OUTPUT <= "0111111";--
			when 51	=> DECODER_OUTPUT <= "1101111";--
			when 52	=> DECODER_OUTPUT <= "1110111";--
			when 53	=> DECODER_OUTPUT <= "1111011";--
			when 54	=> DECODER_OUTPUT <= "0111111";--
			when 55	=> DECODER_OUTPUT <= "1011111";--
			when 56	=> DECODER_OUTPUT <= "1111110";--
			when 57	=> DECODER_OUTPUT <= "1111101";--
			when 58	=> DECODER_OUTPUT <= "0111111";--
			when 59	=> DECODER_OUTPUT <= "1101111";--
			when 60	=> DECODER_OUTPUT <= "1110111";--
			when 61	=> DECODER_OUTPUT <= "1111011";--
			when 62	=> DECODER_OUTPUT <= "0111111";--
			when 63	=> DECODER_OUTPUT <= "1011111";--
			when others => DECODER_OUTPUT <= "1000000";
		end case;
	end process;
end architecture BEHAVIORAL;