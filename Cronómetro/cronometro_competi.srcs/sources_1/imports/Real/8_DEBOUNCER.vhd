----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO & LUIS CASTEDO CEPEDA
-- 
-- Create Date: 17.12.2019
-- Design Name:
-- Module Name: DEBOUNCER - BEHAVIORAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 14.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity DEBOUNCER is
	generic (
		DEBOUNCER_N : positive := 1;
		DEBOUNCER_DELAY : positive := 1000
	);
	port (
		DEBOUNCER_CLOCK : in std_logic;
		DEBOUNCER_INPUT : in std_logic_vector(DEBOUNCER_N-1 downto 0);
		DEBOUNCER_RESET : in std_logic;
		DEBOUNCER_OUTPUT : out std_logic_vector(DEBOUNCER_N-1 downto 0)
	);
end entity DEBOUNCER;

architecture BEHAVIORAL of DEBOUNCER is
	signal DEBOUNCER_INPUT_PRV : std_logic_vector(DEBOUNCER_N-1 downto 0) := (others =>'0');
	signal DEBOUNCER_OUTPUT_SAVE : std_logic_vector(DEBOUNCER_N-1 downto 0) := (others =>'0');
begin
	process(DEBOUNCER_CLOCK, DEBOUNCER_RESET)
		variable COUNTER : integer range 0 to DEBOUNCER_DELAY;
	begin
		if(DEBOUNCER_RESET = '1') then
			DEBOUNCER_OUTPUT	<= (others =>'0');
			DEBOUNCER_INPUT_PRV	<= (others =>'0');
			COUNTER				:= 0;
		elsif rising_edge(DEBOUNCER_CLOCK) then
--			XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			DEBOUNCER_INPUT_PRV <= DEBOUNCER_INPUT;
			if(COUNTER /= 0) then
				DEBOUNCER_OUTPUT <= DEBOUNCER_OUTPUT_SAVE;
				COUNTER := COUNTER - 1;
			else
				if(DEBOUNCER_INPUT_PRV /= DEBOUNCER_OUTPUT_SAVE) then
					DEBOUNCER_OUTPUT <= DEBOUNCER_INPUT_PRV;
					DEBOUNCER_OUTPUT_SAVE <= DEBOUNCER_INPUT_PRV;
					COUNTER := DEBOUNCER_DELAY;
				end if;
			end if;
--			XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
--			DEBOUNCER_OUTPUT <= '0';                 -- Limitar pulso a un ciclo
--			DEBOUNCER_INPUT_PRV <= DEBOUNCER_INPUT;  -- Actualizar valor entrada
--			if(COUNTER /= 0) then                     -- Detecci�n de flancos inhibida
--				COUNTER := COUNTER - 1;
--			else                                     -- Comprobar flancos
--				if(DEBOUNCER_INPUT_PRV /= DEBOUNCER_INPUT) then -- Detectar ambos flancos
--					DEBOUNCER_OUTPUT <= '1';
--					COUNTER := DELAY;
--				end if;
--			end if;
		end if;
	end process;
end architecture BEHAVIORAL;