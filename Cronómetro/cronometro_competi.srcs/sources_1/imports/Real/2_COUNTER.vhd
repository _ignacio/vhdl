----------------------------------------------------------------------------------
-- Company:
-- Engineer: IGNACIO SERNA ESCOBEDO & SANTIAGO VIVES MERINO
-- 
-- Create Date: 03.01.2020
-- Design Name:
-- Module Name: COUNTER - BEHAVIORAL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 16.01.2020
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity COUNTER is
	generic(
		COUNTER_N : positive := 4;
		COUNTER_LIMIT : positive := 9
	);
	port(
		COUNTER_CLOCK : in std_logic;
		COUNTER_RESET : in std_logic;
		COUNTER_ENABLE : in std_logic;
		COUNTER_MODE : in std_logic;
		COUNTER_LOAD : in std_logic_vector(COUNTER_N-1 downto 0);
		COUNTER_OUTPUT : out std_logic_vector(COUNTER_N-1 downto 0);
		COUNTER_CARRIER : out std_logic
	);
end entity COUNTER;

architecture BEHAVIORAL of COUNTER is
	signal S_COUNTER : integer range 0 to COUNTER_LIMIT;
	signal S_CARRIER : std_logic := '0';
begin
	process(COUNTER_CLOCK, COUNTER_RESET, COUNTER_ENABLE)
	begin
		if(COUNTER_RESET='1') then
			S_CARRIER <= '0';
			S_COUNTER <= to_integer(unsigned(COUNTER_LOAD));
		elsif(rising_edge(COUNTER_CLOCK)) then
			if(COUNTER_ENABLE='1') then
				if(COUNTER_MODE='0') then
				--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
					if(S_COUNTER<COUNTER_LIMIT) then
						S_COUNTER <= S_COUNTER+1;
						S_CARRIER <= '0';
					elsif(S_COUNTER=COUNTER_LIMIT) then
						S_COUNTER <= 0;
						S_CARRIER <= '1';
					else
						S_COUNTER <= 0;
						S_CARRIER <= '0';
					end if;
				--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
				else--elsif(COUNTER_MODE='1') then
				--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
					if(S_COUNTER>0) then
						S_COUNTER <= S_COUNTER-1;
						S_CARRIER <= '0';
					elsif(S_COUNTER=0) then
						S_COUNTER <= COUNTER_LIMIT;
						S_CARRIER <= '1';
					else
						S_COUNTER <= 0;
						S_CARRIER <= '0';
					end if;
				--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
				end if;
			elsif(S_CARRIER='1') then
				S_CARRIER <= '0';
			end if;
		end if;
	end process;
	COUNTER_CARRIER	<= S_CARRIER;
	COUNTER_OUTPUT	<= std_logic_vector(to_unsigned(S_COUNTER, COUNTER_N));
end architecture BEHAVIORAL;