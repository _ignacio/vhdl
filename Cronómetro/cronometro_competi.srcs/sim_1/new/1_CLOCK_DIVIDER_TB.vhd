----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.01.2020 18:00:07
-- Design Name: 
-- Module Name: CLOCK_DIVIDER_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CLOCK_DIVIDER_TB is
--  Port ( );
end CLOCK_DIVIDER_TB;

architecture Behavioral of CLOCK_DIVIDER_TB is
    component CLOCK_DIVIDER
        generic(
		INPUT_FREQUENCY : positive := 100000000;
		OUTPUT_FREQUENCY : positive := 10000
        );
        port (CLOCK_DIVIDER_CLOCK_IN  : in std_logic;
              CLOCK_DIVIDER_RESET     : in std_logic;
              CLOCK_DIVIDER_CLOCK_OUT : out std_logic);
    end component;

    signal CLOCK_DIVIDER_CLOCK_IN  : std_logic;
    signal CLOCK_DIVIDER_RESET     : std_logic;
    signal CLOCK_DIVIDER_CLOCK_OUT : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : CLOCK_DIVIDER
    port map (CLOCK_DIVIDER_CLOCK_IN  => CLOCK_DIVIDER_CLOCK_IN,
              CLOCK_DIVIDER_RESET     => CLOCK_DIVIDER_RESET,
              CLOCK_DIVIDER_CLOCK_OUT => CLOCK_DIVIDER_CLOCK_OUT);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLOCK_DIVIDER_CLOCK_IN is really your main clock signal
    CLOCK_DIVIDER_CLOCK_IN <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed

        -- Reset generation
        -- EDIT: Check that CLOCK_DIVIDER_RESET is really your reset signal
        CLOCK_DIVIDER_RESET <= '1';
        wait for 100 ns;
        CLOCK_DIVIDER_RESET <= '0';
        wait for 100 ns;

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;
end Behavioral;





