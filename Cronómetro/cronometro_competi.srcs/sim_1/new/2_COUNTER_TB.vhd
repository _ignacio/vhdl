----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.01.2020 19:02:09
-- Design Name: 
-- Module Name: COUNTER_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity COUNTER_TB is
--  Port ( );
end COUNTER_TB;

architecture Behavioral of COUNTER_TB is

begin


end Behavioral;
-- Testbench automatically generated online
-- at http://vhdl.lapinoo.net
-- Generation date : 16.1.2020 18:04:39 GMT

library ieee;
use ieee.std_logic_1164.all;

entity COUNTER_TB is
--  Port ( );
end COUNTER_TB;


architecture Behavioral of COUNTER_TB is

    component COUNTER
        generic(
		COUNTER_N : positive := 4;
		COUNTER_LIMIT : positive := 9
	);
	port(
		COUNTER_CLOCK : in std_logic;
		COUNTER_RESET : in std_logic;
		COUNTER_ENABLE : in std_logic;
		COUNTER_MODE : in std_logic;
		COUNTER_LOAD : in std_logic_vector(COUNTER_N-1 downto 0);
		COUNTER_OUTPUT : out std_logic_vector(COUNTER_N-1 downto 0);
		COUNTER_CARRIER : out std_logic
	);
    end component;

    signal COUNTER_CLOCK   : std_logic;
    signal COUNTER_RESET   : std_logic;
    signal COUNTER_ENABLE  : std_logic;
    signal COUNTER_MODE    : std_logic;
    signal COUNTER_LOAD    : std_logic_vector (COUNTER_N-1 downto 0);
    signal COUNTER_OUTPUT  : std_logic_vector (COUNTER_N-1 downto 0);
    signal COUNTER_CARRIER : std_logic;

    constant TbPeriod : time := 1000 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : COUNTER
    port map (COUNTER_CLOCK   => COUNTER_CLOCK,
              COUNTER_RESET   => COUNTER_RESET,
              COUNTER_ENABLE  => COUNTER_ENABLE,
              COUNTER_MODE    => COUNTER_MODE,
              COUNTER_LOAD    => COUNTER_LOAD,
              COUNTER_OUTPUT  => COUNTER_OUTPUT,
              COUNTER_CARRIER => COUNTER_CARRIER);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that COUNTER_CLOCK is really your main clock signal
    COUNTER_CLOCK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        COUNTER_ENABLE <= '0';
        COUNTER_MODE <= '0';
        COUNTER_LOAD <= (others => '0');

        -- Reset generation
        -- EDIT: Check that COUNTER_RESET is really your reset signal
        COUNTER_RESET <= '1';
        wait for 100 ns;
        COUNTER_RESET <= '0';
        wait for 100 ns;

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end Behavioral;
